<!DOCTYPE html>
<html lang="en">
<head>

<?php include("head.php"); ?>
<script>verificar_autenticacao(1);</script>

</head>
<body>

<?php include("navbar.php"); ?>

<?php include("conectado.php"); ?>

<div class="pagina-interna">
<div class="container">
  
<div class="row">
<div class="col-md-3 hidden-sm hidden-xs">

<?php include("sidebar.usuario.php"); ?>

</div>
    
<div class="col-md-9">
<div class="row"><div class="col-md-12"><b class="text-primary"><i class="fa fa-file-o fa-fw" aria-hidden="true"></i> Combinações</b><hr class="hr-titulo"></div></div>
<div class="row">
<div class="col-md-12">
  <div class="alert alert-danger display-none" id="alert-danger">Nenhuma combinação ocorreu ainda.</div>
  <div class="alert alert-info" id="alert-info"><i class="fa fa-refresh fa-spin fa-fw"></i> Aguarde, carregando...</div>
</div>
</div>
<div class="display-none" id="grid-resultado"></div>
</div>
    
</div>
</div>
</div>

<?php include("modal.chat.php"); ?>

<div class="display-none" id="template">
<div class="row">
<div class="col-md-6">
  <div class="combinacao">
    <div class="combinacao-carimbo"><img src="img/combinou.png"></div>
    <div class="combinacao-anuncio" style="background-image: url('{{imagemAnuncioUsuario}}');"></div>
    <div class="combinacao-anuncio" style="background-image: url('{{imagemAnuncioAnunciante}}');"></div>
  </div>
</div>
<div class="col-md-6">
  <div class="visible-xs m-top-10"></div>
  O anunciante <b>{{anunciante}}</b> está interessado no seu anúncio nº <a href="<?php print($murl); ?>/anuncio/{{idAnuncioUsuario}}" class="text-danger">#{{idAnuncioUsuario}}</a> e você no anúncio nº <a href="<?php print($murl); ?>/anuncio/{{idAnuncioAnunciante}}" class="text-danger">#{{idAnuncioAnunciante}}</a> dele. Entre em contato agora mesmo.<p></p>
  <button class="btn btn-lg btn-default" onclick="iniciar_chat({{idAnunciante}}, '{{anunciante}}');"><i class="fa fa-comments-o fa-fw" aria-hidden="true"></i> Abrir Conversa</button>
</div>
</div>
<hr>
</div>

<script src="<?php print($murl); ?>/js/bootstrap.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/script.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/painel.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/chat.min.js?<?php print($cache); ?>"></script>
<script>
carregar_combinacoes();
</script>

</body>
</html>