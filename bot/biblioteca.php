<?php

function retorna_id_categoria_imovel($tipo){
  include("database.php");
  $tipo = strtolower($tipo);
  if($tipo == "área") $tipo = "área privativa";

  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  $query = mysqli_query($link, "SELECT id_categoria_imovel FROM tb_categoria_imovel WHERE categoria = '$tipo'");
  $col = mysqli_fetch_assoc($query);
  $resposta = $col["id_categoria_imovel"];
  mysqli_close($link);
  return $resposta;
}

function retorna_id_cidade($cidade, $uf){
  if(strlen($uf) < 2) $uf = "%";
  include("database.php");
  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  $query = mysqli_query($link, "SELECT id_cidade FROM tb_cidade WHERE cidade LIKE '%$cidade%' AND uf LIKE '%$uf%' LIMIT 1");
  $col = mysqli_fetch_assoc($query);
  $resposta = $col["id_cidade"];
  mysqli_close($link);
  return $resposta;
}

function retorna_id_estado_uf($uf){
  include("database.php");
  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  $query = mysqli_query($link, "SELECT id_estado FROM tb_estado WHERE uf = '$uf'");
  $col = mysqli_fetch_assoc($query);
  $resposta = $col["id_estado"];
  mysqli_close($link);
  return $resposta;
}

function retorna_uf_estado_nome($estado){
  include("database.php");
  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  $query = mysqli_query($link, "SELECT uf FROM tb_estado WHERE estado = '$estado'");
  $col = mysqli_fetch_assoc($query);
  $resposta = $col["uf"];
  mysqli_close($link);
  return $resposta;
}

function retorna_id_estado_nome($estado){
  include("database.php");
  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  $query = mysqli_query($link, "SELECT id_estado FROM tb_estado WHERE estado = '$estado'");
  $col = mysqli_fetch_assoc($query);
  $resposta = $col["id_estado"];
  mysqli_close($link);
  return $resposta;
}

function retorna_id_bairro($bairro, $idCidade){
  include("database.php");
  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  $query = mysqli_query($link, "SELECT id_bairro FROM tb_bairro WHERE bairro LIKE '%$bairro%' AND id_cidade = '$idCidade' LIMIT 1");
  //echo("SELECT id_bairro FROM tb_bairro WHERE bairro LIKE '%$bairro%' AND id_cidade = '$idCidade' LIMIT 1"."\n");
  $col = mysqli_fetch_assoc($query);
  $resposta = $col["id_bairro"];
  mysqli_close($link);
  return $resposta;
}

function retorna_id_proposta_imovel($transacao){
  $transacao = strtolower($transacao);
  if($transacao == "venda") return "2";
  if($transacao == "locação") return "3";
}

function retorna_valor_decimal($valor){
  $valor = str_replace("R$ ", "", $valor);
  $valor = str_replace("R$", "", $valor);
  return $valor;
}

function existe_anunciante($idUsuarioParceiro, $idParceiro){
  include("database.php");
  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  $query = mysqli_query($link, "SELECT COUNT(*) AS total FROM tb_usuario WHERE id_usuario_parceiro = '$idUsuarioParceiro' AND id_parceiro = '$idParceiro' AND situacao = 1");
  $col = mysqli_fetch_assoc($query);
  $resposta = $col["total"];
  mysqli_close($link);
  return $resposta;
}

function existe_anuncio($idAnuncioParceiro, $idParceiro){
  include("database.php");
  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  $query = mysqli_query($link, "SELECT COUNT(*) AS total FROM tb_anuncio WHERE id_anuncio_parceiro = '$idAnuncioParceiro' AND id_parceiro = '$idParceiro' AND situacao = 1");
  $col = mysqli_fetch_assoc($query);
  $resposta = $col["total"];
  mysqli_close($link);
  return $resposta;
}

function retorna_id_anunciante_imobex($nome){
  $nome = explode("_", $nome);
  return $nome[1];
}

function retorna_area($area){
  $area = str_replace(" m²", "", $area);
  $area = str_replace("m²", "", $area);
  return $area;
}

function retorna_id_imovel_imobex($id){
  str_replace("_", "", $id);
  return $id;
}

function retorna_anunciante_imobex($nome){
  $nome = explode("_", $nome);
  return $nome[0];
}

function retorna_anunciante_nome($nome){
  $nome = explode(" ", $nome);
  return $nome[0];
}

function retorna_anunciante_sobrenome($nome){
  $nome = explode(" ", $nome);
  array_shift($nome);
  $nome = implode(" ", $nome);
  return retorna_anunciante_imobex($nome);
}

function formata_telefone($telefone){
  $telefone = str_replace("(", "", $telefone);
  $telefone = str_replace(")", "", $telefone);
  $telefone = str_replace(" ", "", $telefone);
  $telefone = str_replace("-", "", $telefone);
  $telefone = str_split($telefone);
  if($telefone[0] == "0") array_shift($telefone);
  array_splice($telefone, 0, 0, "(");
  array_splice($telefone, 3, 0, ")");
  array_splice($telefone, 4, 0, " ");
  array_splice($telefone, 9, 0, "-");
  $telefone = implode("", $telefone);
  return $telefone;
}

function retorna_id_usuario_imobex($id){
  include("database.php");
  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  $query = mysqli_query($link, "SELECT id_usuario FROM tb_usuario WHERE id_usuario_parceiro = '$id' AND id_parceiro = '1' AND situacao = 1");
  $col = mysqli_fetch_assoc($query);
  $resposta = $col["id_usuario"];
  mysqli_close($link);
  return $resposta;
}

function remover_espacos($texto){
  $texto = str_replace("    ", " ", $texto);
  $texto = str_replace("   ", " ", $texto);
  $texto = str_replace("  ", " ", $texto);
  return $texto;
}

function retorna_id_anuncio($idAnuncioParceiro, $idParceiro){
  include("database.php");
  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  $query = mysqli_query($link, "SELECT id_anuncio FROM tb_anuncio WHERE id_anuncio_parceiro = '$idAnuncioParceiro' AND id_parceiro = '$idParceiro' AND situacao = 1");
  $col = mysqli_fetch_assoc($query);
  $resposta = $col["id_anuncio"];
  mysqli_close($link);
  return $resposta;
}
?>