<!DOCTYPE html>
<html lang="en">
<head>

<?php include("head.php"); ?>

</head>
<body>

<?php include("navbar.php"); ?>

<div class="barra-titulo">
<div class="container">
<div class="row">
  <div class="col-md-12 text-left"><b>Anuncie hoje o seu imóvel no Troca Imóvel!</b></div>
</div>
</div>
</div>

<div class="pagina-interna">
<div class="container">
<div class="row">
<div class="col-md-3 hidden-xs">

<?php include("sidebar.pesquisa.php"); ?>

</div>

<div class="col-md-9">
<div class="row">
<div class="col-md-12"><b class="text-primary"><i class="fa fa-file-o fa-fw" aria-hidden="true"></i> Conta Criada</b><hr class="hr-titulo"></div>
<div class="col-md-12">
  <div class="alert alert-success" id="mensagem-conta-criada">Sua conta no Troque Imóvel foi criada com sucesso e já está pronta para ser utilizada. Acesse agora o painel do usuário através do botão abaixo e anuncie o seu primeiro imóvel.</div>
  <a href="http://trocaimovel.com.br/painel.editar.dados" class="btn btn-default btn-lg"><i class="fa fa-sign-in text-danger" aria-hidden="true"></i> Acessar Minha Conta</a>
</div>
</div>
</div>
</div>
</div>

<?php include("footer.php"); ?>

<script src="<?php print($murl); ?>/js/bootstrap.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/script.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/pesquisa.min.js?<?php print($cache); ?>"></script>

</body>
</html>