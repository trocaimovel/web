<footer class="rodape">
<div class="container">

<div class="row hidden-sm hidden-xs">
  <div class="col-md-3">
    <div class="text-center"><img src="<?php print($murl); ?>/img/logo-branca.png" class="logo-branca"></div><p></p>
    <div class="sobre">Todas as formas de negociar o seu imóvel, em um só aplicativo. Ao curtir um imóvel e o dono deste imóvel também curtir o seu, vocês receberão uma notificação. E, automaticamente, será criado um chat entre os dois para que comecem a negociação direta e sem complicação.</div>
  </div>
  <div class="col-md-3 text-left">
    <h3>Troca Imóvel</h3>
    <a href="<?php print($murl); ?>/quemsomos" class="link"><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> Conheça o Troca Imóvel</a>
    <a href="<?php print($murl); ?>/sobre" class="link"><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> Como Funciona</a>
    <a href="<?php print($murl); ?>/parceiros"class="link"><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> Parceiros</a>
  </div>
  <div class="col-md-3">
    <h3>Política e Contrato</h3>
    <a href="<?php print($murl); ?>/privacidade" class="link"><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> Política de Privacidade</a>
    <a href="<?php print($murl); ?>/docs/" class="link"><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> Contrato de Anúncio</a>
  </div>
  <div class="col-md-3">
    <h3>Troca Imóvel Mobile</h3>
    <a href="https://itunes.apple.com/us/app/troca-imovel/id1137616244" class="link"><i class="fa fa-apple fa-fw" aria-hidden="true"></i> Troca Imóvel para iOS</a>
    <a href="https://play.google.com/store/apps/details?id=com.troca.imovel" class="link"><i class="fa fa-android fa-fw" aria-hidden="true"></i> Troca Imóvel para Android</a>
  </div>
</div>

<div class="row visible-sm visible-xs">
<div class="col-xs-12 text-center">
  <img src="<?php print($murl); ?>/img/logo-branca.png" class="logo-branca">
</div>
<div class="col-xs-12 m-top-15 text-center">
  <div class="rodape-menu">
    <a href="<?php print($murl); ?>/quemsomos">Troca Imóvel</a>
    <a href="<?php print($murl); ?>/sobre">Como Funciona</a>
  </div>
</div>
<div class="col-xs-12 m-top-15 text-center">
  <a href="http://fb.com/trocaimovel"><i class="fa fa-facebook-square fa-2x"></i></a>
  <a href="http://twitter.com/trocaimovel"><i class="fa fa-twitter-square fa-2x"></i></a>
  <a href="http://instagram.com/trocaimovel"><i class="fa fa-instagram fa-2x"></i></a>
</div>
</div>

</div>
</footer>
