<!DOCTYPE html>
<html lang="en">
<head>

<?php include("head.php"); ?>
<script>verificar_autenticacao(1);</script>

</head>
<body>

<?php include("navbar.php"); ?>

<?php include("conectado.php"); ?>

<div class="pagina-interna">
<div class="container">
  
<div class="row">
<div class="col-md-3 hidden-sm hidden-xs">

<?php include("sidebar.usuario.php"); ?>

</div>
    
<div class="col-md-9">
<div class="row"><div class="col-md-12"><b class="text-primary"><i class="fa fa-inbox fa-fw" aria-hidden="true"></i> Caixa de Mensagens</b><hr class="hr-titulo"></div></div>

  <div class="alert alert-info" id="alert-info"><i class="fa fa-refresh fa-spin fa-fw"></i> Aguarde, carregando...</div>
  <div class="panel panel-default display-none" id="grid-mensagens">
  <table class="table table-bordered table-striped" id="table-grid"></table>
  </div>

</div>
</div>
</div>
</div>

<?php include("modal.chat.php"); ?>

<div class="display-none" id="template">
<div class="pull-left p-top-1" id="correspondente">{{envelope}}{{correspondente}}</div>
<div class="pull-right">
  <button type="button" class="btn btn-xs btn-primary" cid="{{idCorrespondente}}" onclick="iniciar_chat($(this).attr('cid'), $('#correspondente').text());"><i class="fa fa-comments-o fa-fw"></i> abrir conversa</button> 
  <button type="button" class="btn btn-xs btn-danger" cid="{{idCorrespondente}}" onclick="confirmar_remocao_conversa($(this).attr('cid'));"><i class="fa fa-times fa-fw"></i> apagar</button>
</div>
</div>

<script src="<?php print($murl); ?>/js/bootstrap.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/script.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/painel.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/chat.min.js?<?php print($cache); ?>"></script>

<script>
carregar_grid_mensagens();
</script>

</body>
</html>