<!DOCTYPE html>
<html lang="en">
<head>

<?php include("head.php"); ?>

<?php include("metatags.anuncio.php") ?>

<link href="<?php print($murl); ?>/plugin/pgwslideshow/pgwslideshow.css" rel="stylesheet">

</head>
<body>
<div class="pagina">
  
<?php include_once("analyticstracking.php") ?>

<?php include("navbar.php"); ?>

<div class="barra-titulo">
<div class="container">
<div class="row">
  <div class="col-md-6 col-sm-6 text-left"><b id="titulo"></b></div>
  <div class="col-md-6 col-sm-6 text-right hidden-xs">Anunciante: <span id="anunciante"></span></div>
  <input type="hidden" id="id-anuncio">
  <input type="hidden" id="id-anunciante">
</div>
</div>
</div>

<div class="pagina-interna">
<div class="container">
<div class="row">
  
<div class="col-md-3 hidden-sm hidden-xs">
<div class="sidebar-pesquisa m-bottom-30">
<div class="row">
<div class="col-md-12">
  <label>Seu nome:</label>
  <input type="text" class="form-control" id="contato-nome" name="contatoNome" maxlength="50">
</div>
<div class="col-md-12 m-top-10">
  <label>E-mail para contato:</label>
  <input type="text" class="form-control" id="contato-email" name="contatoEmail" maxlength="50">
</div>
<div class="col-md-12 m-top-10">
  <label>Telefone para contato:</label>
  <input type="text" class="form-control" id="contato-telefone" name="contatoTelefone">
</div>
<div class="col-md-12 m-top-10">
  <label>Mensagem:</label>
  <textarea class="form-control" rows="3" id="contato-mensagem" name="contatoMensagem" maxlength="200"></textarea>
</div>
<div class="col-xs-12 m-top-15"><button class="btn btn-danger btn-lg btn-block btn-submit" onclick="enviar_email_anuncio($('#id-anuncio').val());"><i class="fa fa-paper-plane fa-fw" aria-hidden="true"></i> Enviar Mensagem</button></div>
</div>
</div>
<button class="btn btn-primary btn-lg btn-block margin-0 display-none" id="btn-chat" onclick="iniciar_chat($('#id-anunciante').val(), $('#anunciante').text());"><i class="fa fa-comments fa-fw" aria-hidden="true"></i> Conversar no Chat</button>
<!--<img src="http://placehold.it/250x250" class="ad-250">-->
</div>
    
<div class="col-md-9">
<div class="alert alert-danger display-none" id="alert-danger">O anúncio que você está tentando acessar não existe mais.</div>
<div class="alert alert-info" id="alert-loading"><i class="fa fa-refresh fa-spin fa-fw"></i> Aguarde, carregando...</div>
<div class="display-none" id="anuncio">
<div class="row">
<div class="col-md-12">
  <ul class="nav nav-tabs">
    <li id="li-dados" class="active"><a onclick="abrir_aba('dados');" href="#">Dados<span class="hidden-xs"> do Imóvel</span></a></li>
    <li id="li-video" class="display-none-important"><a onclick="$('#modal-youtube').modal('show');" href="#"><i class="fa fa-youtube-play fa-fw text-danger" aria-hidden="true"></i> Vídeo</a></li>
    <div class="dropdown pull-right">
      <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        <i class="fa fa-ban fa-fw text-danger"></i> Denunciar <span class="caret"></span>
      </button>
      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
        <li><a onclick="denunciar_anuncio($('#id-anuncio').val(), 1);">Anúncio falso</a></li>
        <li><a onclick="denunciar_anuncio($('#id-anuncio').val(), 2);">Imóvel indisponível</a></li>
        <li><a onclick="denunciar_anuncio($('#id-anuncio').val(), 3);">Anunciante não responde</a></li>
      </ul>
    </div>
    <div class="pull-right m-right-5">
      <a target="_blank" class="btn btn-default" id="btn-share-facebook"><i class="fa fa-facebook-square text-facebook"></i><span class="hidden-xs"> Compartilhar</span></a>
    </div>
    <div class="pull-right m-right-5">
      <a target="_blank" class="btn btn-default" id="btn-share-twitter"><i class="fa fa-twitter text-twitter"></i></a>
    </div>
    <div class="pull-right hidden-xs m-right-5">
      <a target="_blank" class="btn btn-default" id="btn-share-google"><i class="fa fa-google-plus text-google"></i></a>
    </div>
  </ul>
</div>
</div>

<div class="row">
<div class="col-md-12">
  <div class="text-primary text-uppercase m-top-30"><small id="localizacao"></small></div>
  <div class="m-top-10" id="descricao"></div>
</div>
<div class="col-md-8"><div class="m-top-30">
  <ul class="pgwSlideshow"></ul>
  <img class="img-anuncio-sem-imagem display-none" src="http://trocaimovel.com.br/img/sem-imagem.gif" id="sem-imagem">
</div></div>

<div class="col-md-4">
<button class="btn btn-danger btn-lg btn-block text-uppercase m-top-30 display-none" id="btn-adicionar-desejo" onclick="exibir_modal_desejo();"><i class="fa fa-check fa-fw" aria-hidden="true"></i> <small>quero negociar</small></button>
<table class="table table-striped table-bordered text-primary text-uppercase m-top-30">
  <tr><td width="50%"><i class="fa fa-bookmark fa-fw text-primary" aria-hidden="true"></i> <small id="proposta"></small></td><td><i class="fa fa-home fa-fw text-danger" aria-hidden="true"></i> <small class="text-lowercase" id="metragem"></small></td></tr>
  <tr><td width="50%"><i class="fa fa-bed fa-fw text-danger" aria-hidden="true"></i> <small id="n-quartos"></small></td><td><i class="fa fa-bed fa-fw text-danger" aria-hidden="true"></i> <small id="n-suites"></small></td></tr>
  <tr><td width="50%"><i class="fa fa-shower fa-fw text-danger" aria-hidden="true"></i> <small id="n-banheiros"></small></td><td><i class="fa fa-car fa-fw text-danger" aria-hidden="true"></i> <small id="n-vagas"></small></td></tr>
</table>
<table class="table table-striped table-bordered text-primary text-uppercase m-top-30">
  <tr><td width="50%"><small id="valor">valor:</small></td><td><small id="valor-imovel"></small></td></tr>
  <tr><td width="50%"><small>iptu:</small></td><td><small id="valor-iptu"></small></td></tr>
  <tr><td width="50%"><small>condimínio:</small></td><td><small id="valor-condominio"></small></td></tr>
</table>
<table class="table table-striped table-bordered text-primary text-uppercase m-top-30">
  <tr><td width="50%"><i class="fa fa-phone fa-fw"></i> <small>contato:</small></td><td><small class="text-lowercase" id="telefone-numero">não informado</small></td></tr>
</table>
<div class="text-center">
  <img class="display-none img-logo-anunciante" id="img-logo-anunciante">
</div>
</div>

<div class="col-md-12 m-top-30">
<b>Localização:</b><hr class="hr-titulo">
<div class="row">
  <div class="col-md-6 col-sm-6 col-xs-12">
    <img id="mapa-cidade" class="mapa-google">
  </div>
  <div class="col-md-6 col-sm-6 col-xs-12 hidden-xs">
    <img id="mapa-bairro" class="mapa-google">
  </div>
</div>
</div>

<div class="col-md-12 m-top-30" id="imoveis-relacionados">
<b>Imóveis relacionados:</b><hr class="hr-titulo">  
<div class="row" id="grid-relacionados"></div>  
</div>

</div>
</div>
</div>
</div>
</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-youtube">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <i class="fa fa-youtube-play fa-fw text-danger" aria-hidden="true"></i> <b>Youtube Vídeo</b>
      </div>
      <div class="modal-body">
        <iframe width="100%" height="315" frameborder="0" id="youtube-iframe" allowfullscreen></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times fa-fw" aria-hidden="true"></i> Fechar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-desejo">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <b>Qual dos seus imóveis você gostaria de negociar com este?</b>
      </div>
      <div class="modal-body">
        <div class="alert alert-info margin-0 display-none" id="alert-info-desejo">Você não tem nenhum imóvel anunciado.</div>
        <div class="display-none" id="grid-anuncios">
        <table class="table table-bordered table-striped margin-0" id="table-grid"></table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times fa-fw" aria-hidden="true"></i> Fechar</button>
      </div>
    </div>
  </div>
</div>

<?php include("modal.chat.php"); ?>

<div class="display-none" id="template-desejo">
<div class="pull-left p-top-1">{{titulo}}</div>
<div class="pull-right">
  <button class="btn btn-xs btn-danger" rid="{{id}}" onclick="adicionar_desejo($(this).attr('rid'), $('#id-anuncio').val())"><i class="fa fa-check fa-fw"></i> negociar</button>
</div>
</div>

<div class="display-none" id="template-relacionado">
<div class="col-md-4 col-sm-4 col-xs-12">
<div class="borda-cinza">
  <a href="<?php print($murl); ?>/anuncio/{{id}}"><div class="destaque-quadro">
    <div class="destaque-imovel-valor">R$ {{valor}}</div>
    <div class="destaque-imovel" style="background-image: url({{imagem}});"></div>
  </div></a>
</div>
</div>
<div class="visible-xs m-top-30"></div>
</div>

</div>

<?php include("footer.php"); ?>

<script src="<?php print($murl); ?>/js/bootstrap.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.money.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/plugin/pgwslideshow/pgwslideshow.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/script.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/anuncio.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/chat.min.js?<?php print($cache); ?>"></script>

</body>
</html>