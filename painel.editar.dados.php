<!DOCTYPE html>
<html lang="en">
<head>

<?php include("head.php"); ?>
<script>verificar_autenticacao(1);</script>

</head>
<body>

<?php include("navbar.php"); ?>

<?php include("conectado.php"); ?>

<div class="pagina-interna m-bottom-30">
<div class="container">
<div class="row">
<div class="col-md-3 hidden-sm hidden-xs">

<?php include("sidebar.usuario.php"); ?>

</div>

<div class="col-md-9">
<div class="row"><div class="col-md-12">
  <div class="alert alert-info" id="alert-loading"><i class="fa fa-refresh fa-spin fa-fw"></i> Aguarde, carregando...</div>
</div></div>
<div class="row display-none" id="formulario-editar-dados">
<div class="col-md-12"><b class="text-primary"><i class="fa fa-file-o fa-fw" aria-hidden="true"></i> Alterar Dados</b><hr class="hr-titulo"></div>

<div class="col-md-12 m-bottom-20">
<ul class="nav nav-tabs">
  <li class="active"><a href="<?php print($murl); ?>/painel.editar.dados"><span class="hidden-xs">Meus</span> Dados</a></li>
  <li><a href="<?php print($murl); ?>/painel.alterar.logo">Logotipo</a></li>
  <li><a href="<?php print($murl); ?>/painel.editar.senha"><span class="hidden-xs">Alterar</span> Senha</a></li>
  <li class="hidden-xs"><a href="<?php print($murl); ?>/painel.remover.conta">Remover Conta</a></li>
</ul>
</div>

<div class="col-md-4">
  <label>Nome:</label>
  <input type="text" class="form-control text-capitalize" id="nome" name="nome" maxlength="20">
</div>
<div class="col-md-4">
  <p class="visible-xs visible-sm"></p>
  <label>Último sobrenome:</label>
  <input type="text" class="form-control text-capitalize" id="sobrenome" name="sobrenome" maxlength="20">
</div>
<div class="col-md-4">
  <p class="visible-xs visible-sm"></p>
  <label>Data de nascimento:</label>
  <input type="text" class="form-control" id="nascimento" name="nascimento">
</div>
<div class="col-md-3 m-top-10">
  <label>Sexo:</label>
  <select class="form-control" id="sexo" name="sexo">
    <option value="M">masculino</option>
    <option value="F">feminino</option>
  </select>
</div>
<div class="col-md-3 m-top-10">
  <label>Estado:</label>
  <select class="form-control" id="select-estado" name="idEstado" onchange="carregar_select_cidade(null, $(this).val());"></select>
</div>
<div class="col-md-6 m-top-10">
  <label>Cidade:</label>
  <select class="form-control" id="select-cidade" name="idCidade"></select>
</div>
<div class="col-md-3 m-top-10">
  <label>RG:</label>
  <input type="text" class="form-control" id="rg" name="rg" placeholder="somente números" maxlength="15">
</div>
<div class="col-md-3 m-top-10">
  <label>CPF:</label>
  <input type="text" class="form-control" id="cpf" name="cpf" maxlength="14">
</div>
<div class="col-md-6 m-top-10">
  <label>E-mail de cadastro:</label>
  <input type="text" class="form-control text-lowercase" maxlength="50" id="email-cadastro" name="email-cadastro" disabled>
</div>
<div class="col-md-6 m-top-10">
  <label>E-mail para contato:</label>
  <input type="text" class="form-control text-lowercase" id="email-contato" name="emailContato" maxlength="50">
</div>
<div class="col-md-3 m-top-10">
  <label>Telefone para contato:</label>
  <input type="text" class="form-control" id="telefone" name="telefone" pattern="\([0-9]{2}\)[\s][0-9]{4}-[0-9]{4,5}">
</div>
<div class="col-md-3 m-top-10">
  <label>Exibir telefone no anúncio:</label>
  <select class="form-control" id="exibir-telefone" name="exibirTelefone">
    <option value="1">sim</option>
    <option value="0">não</option>
  </select>
</div>

<div class="col-md-12">
  <hr>
  <div class="alert alert-success display-none" id="alert-success"></div>
  <div class="alert alert-danger display-none" id="alert-danger"></div>
  <div class="alert alert-warning display-none" id="alert-warning"></div>
  <div class="btn btn-danger btn-lg" id="btn-submit" onclick="editar_dados_usuario();"><i class="fa fa-check fa-fw" aria-hidden="true"></i> Alterar Dados</div>
</div>

</div>
</div>
</div>
</div>
</div>

<script src="<?php print($murl); ?>/js/bootstrap.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/script.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/painel.min.js?<?php print($cache); ?>"></script>
<script>
carregar_dados_usuario();
</script>

</body>
</html>