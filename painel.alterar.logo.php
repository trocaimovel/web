<!DOCTYPE html>
<html lang="en">
<head>

<?php include("head.php"); ?>
<script>verificar_autenticacao(1);</script>

</head>
<body>

<?php include("navbar.php"); ?>

<?php include("conectado.php"); ?>

<div class="pagina-interna m-bottom-30">
<div class="container">
<div class="row">
<div class="col-md-3 hidden-sm hidden-xs">

<?php include("sidebar.usuario.php"); ?>

</div>

<div class="col-md-9">
<div class="row">
<div class="col-md-12"><b class="text-primary"><i class="fa fa-file-o fa-fw" aria-hidden="true"></i> Alterar Dados</b><hr class="hr-titulo"></div>

<div class="col-md-12 m-bottom-20">
<ul class="nav nav-tabs">
  <li><a href="<?php print($murl); ?>/painel.editar.dados"><span class="hidden-xs">Meus</span> Dados</a></li>
  <li class="active"><a href="<?php print($murl); ?>/painel.alterar.logo">Logotipo</a></li>
  <li><a href="<?php print($murl); ?>/painel.editar.senha"><span class="hidden-xs">Alterar</span> Senha</a></li>
  <li class="hidden-xs"><a href="<?php print($murl); ?>/painel.remover.conta">Remover Conta</a></li>
</ul>
</div>

<form id="formulario-alterar-logotipo" method="post" enctype="multipart/formdata">
<input type="hidden" id="token" name="token">

<div class="col-md-8">
  <label>Selecione uma imagem:</label>
  <input type="file" class="filestyle" data-buttonName="btn-primary" data-buttonText="" id="upload" name="upload">
  
  <hr>
  <div class="alert alert-success display-none" id="alert-success"></div>
  <div class="alert alert-danger display-none" id="alert-danger"></div>
  <div class="alert alert-warning display-none" id="alert-warning"></div>
  <div class="alert alert-info display-none" id="alert-loading"><i class="fa fa-refresh fa-spin fa-fw"></i> Aguarde, enviando...</div>
  <button type="submit" class="btn btn-danger btn-lg" id="btn-submit"><i class="fa fa-check fa-fw" aria-hidden="true"></i> Enviar Logotipo</button>
  <button type="button" class="btn btn-default btn-lg pull-right" onclick="remover_logotipo()"><i class="fa fa-trash-o fa-fw text-danger" aria-hidden="true"></i> Remover Atual</button>
</div>
<div class="col-md-4">
  <div class="panel panel-default">
    <div class="panel-body text-center" id="logotipo"></div>
  </div>
</div>

</form>

</div>
</div>
</div>
</div>
</div>

<script src="<?php print($murl); ?>/js/bootstrap.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/script.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/painel.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/bootstrap.filestyle.min.js"></script>
<script>
carregar_logotipo_usuario();
$('file').filestyle();
</script>

</body>
</html>