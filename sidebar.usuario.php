<div class="list-group m-bottom-30">
  <a href="<?php print($murl); ?>/painel.adicionar.anuncio" type="button" class="list-group-item"><i class="fa fa-bullhorn fa-fw text-danger" aria-hidden="true"></i> Anunciar imóvel</a>
  <a href="<?php print($murl); ?>/painel.gerenciar.mensagens" type="button" class="list-group-item"><i class="fa fa-inbox fa-fw text-primary" aria-hidden="true"></i> Novas mensagens <span class="badge total-novas-mensagens">0</span></a>
  <a href="<?php print($murl); ?>/painel.gerenciar.anuncios" type="button" class="list-group-item">Meus anúncios <span class="badge total-anuncios">0</span></a>
  <a href="<?php print($murl); ?>/painel.combinacoes" type="button" class="list-group-item">Combinações</a>
  <a href="<?php print($murl); ?>/painel.recomendados" type="button" class="list-group-item">Recomendados</a>
</div>

<div class="list-group">
  <a href="<?php print($murl); ?>/painel.adicionar.interesse" type="button" class="list-group-item"><i class="fa fa-plus-square fa-fw text-danger" aria-hidden="true"></i> Adicionar interesse</a>
  <a href="<?php print($murl); ?>/painel.gerenciar.interesses" type="button" class="list-group-item">Meus interesses <span class="badge total-interesses">0</span></a>
</div>