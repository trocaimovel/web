<script type="text/javascript">
window.fbAsyncInit = function() {
  FB.init({
    appId: 1112856288771500,
    xfbml: true,
    version: 'v2.0'
  });
};

(function(d, s, id){
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) {return;}
js = d.createElement(s); js.id = id;
js.src = "//connect.facebook.net/en_US/sdk.js";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<div class="topo">
  <div class="topo-container">
    
  <div class="topo-menu-superior" id="menu-superior-padrao">
    <div class="container">
      <div class="row hidden-sm hidden-xs">
        <div class="col-md-4 text-left"><a href="<?php print($murl); ?>"><img src="<?php print($murl); ?>/img/logo.png" class="logo"></a></div>
        <div class="col-md-8 text-right"></div>
      </div>
    </div>
    <div class="row visible-sm visible-xs">
      <div class="col-md-12 text-center"><a href="<?php print($murl); ?>"><img src="<?php print($murl); ?>/img/logo.png" class="logo"></a></div>
    </div>
  </div>
  
  <div class="topo-menu-superior display-none" id="menu-superior-desconectado">
    <div class="container">
      <div class="row hidden-sm hidden-xs">
        <div class="col-md-4 text-left"><a href="<?php print($murl); ?>"><img src="<?php print($murl); ?>/img/logo.png" class="logo"></a></div>
        <div class="col-md-8 text-right">
          <div class="m-top-15">
            <a class="btn btn-danger btn-lg" data-toggle="modal" data-target=".modal-login"><i class="fa fa-bullhorn fa-fw" aria-hidden="true"></i> Anunciar Imóvel</a>
            <a href="<?php print($murl); ?>/cadastro" class="btn btn-default btn-lg">Criar Conta</a>
            <a class="btn btn-default btn-lg" data-toggle="modal" data-target=".modal-login"><i class="fa fa-user text-danger fa-fw" aria-hidden="true"></i> Logar</a>
          </div>
        </div>
      </div>
    </div>
    <div class="row visible-sm visible-xs">
      <div class="col-md-12 text-center"><a href="<?php print($murl); ?>"><img src="<?php print($murl); ?>/img/logo.png" class="logo"></a></div>
    </div>
  </div>
  
  <div class="topo-menu-superior display-none" id="menu-superior-conectado">
    <div class="container">
      <div class="row hidden-sm hidden-xs">
        <div class="col-md-4 text-left"><a href="<?php print($murl); ?>"><img src="<?php print($murl); ?>/img/logo.png" class="logo"></a></div>
        <div class="col-md-8 text-right">
          <div class="m-top-2">
            <div class="text-primary m-bottom-5"><i class="fa fa-user fa-fw text-danger"></i> Você está conectado como: <b class="nome-usuario">Usuário</b></div>
            <div>
              <a href="<?php print($murl); ?>/painel.adicionar.anuncio" class="btn btn-danger btn-lg"><i class="fa fa-bullhorn fa-fw" aria-hidden="true"></i> Anunciar</a>
              <a href="<?php print($murl); ?>/painel.gerenciar.mensagens" class="btn btn-default btn-lg"><i class="fa fa-inbox fa-fw text-primary" aria-hidden="true"></i> Mensagens</a>
              <a href="<?php print($murl); ?>/painel.gerenciar.anuncios" class="btn btn-default btn-lg">Meus Anúncios</a>
              <a href="<?php print($murl); ?>/painel.editar.dados" class="btn btn-default btn-lg">Meus Dados</a>
              <button class="btn btn-default btn-lg" onclick="desconectar_usuario();"><i class="fa fa-power-off text-danger fa-fw" aria-hidden="true"></i> Sair</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row visible-sm visible-xs">
      <div class="col-md-12 text-center"><a href="<?php print($murl); ?>"><img src="<?php print($murl); ?>/img/logo.png" class="logo"></a></div>
    </div>
  </div>
  
  <div class="topo-menu-inferior">
    <div class="container">
      <div class="row hidden-sm hidden-xs">
        <div class="col-md-6 text-left">
          <a href="<?php print($murl); ?>" class="menu-topo-item"><i class="fa fa-home fa-fw" aria-hidden="true"></i> Home</a>
          <a href="<?php print($murl); ?>/quemsomos" class="menu-topo-item">Troca Imóvel</a>
          <a href="<?php print($murl); ?>/sobre" class="menu-topo-item">Como Funciona</a>
          <a href="<?php print($murl); ?>/resultado.pesquisa?idProposta=1" class="menu-topo-item">Trocar</a>
          <a href="<?php print($murl); ?>/resultado.pesquisa?idProposta=2" class="menu-topo-item">Vender</a>
          <a href="<?php print($murl); ?>/resultado.pesquisa?idProposta=3" class="menu-topo-item">Alugar</a>
        </div>
        <div class="col-md-6 text-right menu-social">
          <a href="http://fb.com/trocaimovel"><i class="fa fa-facebook-square fa-2x"></i></a>
          <a href="http://twitter.com/trocaimovel"><i class="fa fa-twitter-square fa-2x"></i></a>
          <a href="http://instagram.com/trocaimovel"><i class="fa fa-instagram fa-2x"></i></a>
        </div>
      </div>
      <div class="row visible-sm visible-xs">
        <div class="col-xs-12 text-left display-none" id="menu-inferior-padrao">
          <div class="menu-topo-item" onclick="$('#modal-menu').modal('show');"><i class="fa fa-bars fa-fw"></i> Menu</div>
          <div class="menu-topo-item" onclick="$('#modal-login').modal('show');"><i class="fa fa-user fa-fw"></i> Acessar</div>
        </div>
        <div class="col-xs-12 text-left display-none" id="menu-inferior-conectado">
          <div class="menu-topo-item" onclick="$('#modal-menu-conectado').modal('show');"><i class="fa fa-bars fa-fw"></i> Menu</div>
          <div class="menu-topo-item" onclick="desconectar_usuario();"><i class="fa fa-user fa-fw"></i> Sair</div>
        </div>
      </div>
    </div>
  </div>
  </div>
</div>

<div class="modal fade modal-login" tabindex="-1" role="dialog" id="modal-login">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title text-center"><b class="text-primary">Autenticação</b></h5>
      </div>
      <div class="modal-body" id="formulario-autenticacao">
        <div class="row">
          <div class="col-md-12">
            <label>E-mail de cadastro:</label>
            <input type="text" class="form-control text-lowercase" name="login-email" id="login-email" maxlength="50">
          </div>
          <div class="col-md-12 m-top-10">
            <label>Senha:</label>
            <input type="password" class="form-control" name="login-senha" id="login-senha" maxlength="20">
          </div>
          <div class="col-md-12">
            <hr>
            <div class="row">
              <div class="col-xs-12">
                <div class="alert alert-success display-none" id="alert-login-success"></div>
                <div class="alert alert-danger display-none" id="alert-login-danger"></div>
                <div class="alert alert-warning display-none" id="alert-login-warning"></div>
              </div>
              <div class="col-xs-6"><button class="btn btn-danger btn-lg btn-block" onclick="autenticar_usuario();" id="btn-login-submit"><i class="fa fa-user fa-fw" aria-hidden="true"></i> Acessar</button></div>
              <div class="col-xs-6"><button class="btn btn-default btn-lg btn-block" onclick="exibir_modal_senha();"><small>Lembrar Senha</small></button></div>
              <div class="col-xs-12">
                <hr>
                <div class="alert alert-info display-none" id="facebook-alert">Após permitir o acesso, clique novamente no botão para logar.</div>
                <button class="btn btn-facebook btn-lg btn-block" onclick="autenticar_facebook();"><i class="fa fa-facebook-square fa-fw" aria-hidden="true"></i> <small>Logar com o Facebook</small></button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-body display-none" id="formulario-senha">
        <div class="row">
          <div class="col-md-12">
            <label>E-mail de cadastro:</label>
            <input type="text" class="form-control text-lowercase" name="senha-email" id="senha-email" maxlength="50">
          </div>
          <div class="col-md-12 m-top-10">
            <label>CPF:</label>
            <input type="text" class="form-control" name="senha-cpf" id="senha-cpf">
          </div>
          <div class="col-md-12">
            <hr>
            <div class="row">
              <div class="col-xs-12">
                <div class="alert alert-success display-none" id="alert-senha-success"></div>
                <div class="alert alert-danger display-none" id="alert-senha-danger"></div>
                <div class="alert alert-warning display-none" id="alert-senha-warning"></div>
              </div>
              <div class="col-xs-12"><button class="btn btn-danger btn-lg btn-block" onclick="enviar_email_senha();" id="btn-senha-submit"><i class="fa fa-envelope fa-fw" aria-hidden="true"></i> Enviar Nova Senha</button></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-menu">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <i class="fa fa-bars fa-fw text-danger" aria-hidden="true"></i> <b>Menu</b>
      </div>
      <div class="modal-body m-bottom-0">
        <div class="list-group m-bottom-0">
          <a href="http://trocaimovel.com.br/resultado.pesquisa?idProposta=1" class="list-group-item"><i class="fa fa-caret-right fa-fw text-danger" aria-hidden="true"></i> Imóveis para trocar</a>
          <a href="http://trocaimovel.com.br/resultado.pesquisa?idProposta=2" class="list-group-item"><i class="fa fa-caret-right fa-fw text-danger" aria-hidden="true"></i> Imóveis para vender</a>
          <a href="http://trocaimovel.com.br/resultado.pesquisa?idProposta=3" class="list-group-item"><i class="fa fa-caret-right fa-fw text-danger" aria-hidden="true"></i> Imóveis para alugar</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-menu-conectado">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <i class="fa fa-bars fa-fw text-danger" aria-hidden="true"></i> <b>Menu</b>
      </div>
      <div class="modal-body m-bottom-0">
        <div class="list-group m-bottom-30">
          <a href="<?php print($murl); ?>/painel.adicionar.anuncio" type="button" class="list-group-item"><i class="fa fa-bullhorn fa-fw text-danger" aria-hidden="true"></i> Anunciar imóvel</a>
          <a href="<?php print($murl); ?>/painel.gerenciar.mensagens" type="button" class="list-group-item"><i class="fa fa-inbox fa-fw text-primary" aria-hidden="true"></i> Novas mensagens <span class="badge total-novas-mensagens">0</span></a>
          <a href="<?php print($murl); ?>/painel.gerenciar.anuncios" type="button" class="list-group-item">Meus anúncios <span class="badge total-anuncios">0</span></a>
          <a href="<?php print($murl); ?>/painel.combinacoes" type="button" class="list-group-item">Combinações</a>
          <a href="<?php print($murl); ?>/painel.recomendados" type="button" class="list-group-item">Recomendados</a>
        </div>        
        <div class="list-group m-bottom-0">
          <a href="<?php print($murl); ?>/painel.adicionar.interesse" type="button" class="list-group-item"><i class="fa fa-plus-square fa-fw text-danger" aria-hidden="true"></i> Adicionar interesse</a>
          <a href="<?php print($murl); ?>/painel.gerenciar.interesses" type="button" class="list-group-item">Meus interesses <span class="badge total-interesses">0</span></a>
          <a href="<?php print($murl); ?>/painel.editar.dados" type="button" class="list-group-item"><i class="fa fa-user fa-fw text-danger" aria-hidden="true"></i> Meus dados</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="modal-alerta">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header"><b>Alerta!</b></div>
      <div class="modal-body" id="modal-alerta-mensagem"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btn-alerta-ok"><i class="fa fa-check fa-fw"></i> Ok</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="modal-confirmacao">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header"><b>Confirmação!</b></div>
      <div class="modal-body" id="modal-confirmacao-mensagem"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btn-confirmacao-positivo"><i class="fa fa-check fa-fw"></i> Sim</button>
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban fa-fw text-danger"></i> Cancelar</button>
      </div>
    </div>
  </div>
</div>