<!DOCTYPE html>
<html lang="en">
<head>

<?php include("head.php"); ?>
<script>verificar_autenticacao(1);</script>

</head>
<body>

<?php include("navbar.php"); ?>

<?php include("conectado.php"); ?>

<div class="pagina-interna">
<div class="container">
  <div class="row">
    <div class="col-md-3 hidden-sm hidden-xs">
      <?php include("sidebar.usuario.php"); ?>
    </div>
    <div class="col-md-9">
      <div class="row"><div class="col-md-12"><b class="text-primary"><i class="fa fa-file-o fa-fw" aria-hidden="true"></i> Meus Anúncios</b><hr class="hr-titulo"></div></div>

      <div class="alert alert-info" id="alert-info"><i class="fa fa-refresh fa-spin fa-fw"></i> Aguarde, carregando...</div>
      <div class="panel panel-default display-none" id="grid-anuncios">
      <table class="table table-bordered table-striped" id="table-grid"></table>
      </div>

    </div>
  </div>
</div>
</div>

<div class="template display-none" id="template">
<div class="pull-left p-top-1">{{titulo}}</div>
<div class="pull-right">
  <a href="<?php print($murl); ?>/anuncio/{{id}}" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-eye fa-fw"></i> visualizar</a>
  <a href="<?php print($murl); ?>/painel.editar.anuncio/{{id}}" class="btn btn-xs btn-primary"><i class="fa fa-pencil fa-fw"></i> editar</a>
  <button class="btn btn-xs btn-danger" rid="{{id}}" onclick="confirmar_remocao_anuncio($(this).attr('rid'));"><i class="fa fa-times fa-fw"></i> apagar</button>
</div>
</div>

<script src="<?php print($murl); ?>/js/bootstrap.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/script.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/painel.min.js?<?php print($cache); ?>"></script>
<script>
carregar_grid_anuncios();
</script>

</body>
</html>