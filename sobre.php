<!DOCTYPE html>
<html lang="en">
<head>

<?php include("head.php"); ?>

<?php include("metatags.home.php") ?>

</head>
<body>

<?php include_once("analyticstracking.php") ?>

<?php include("navbar.php"); ?>

<div class="barra-titulo">
<div class="container">
<div class="row">
  <div class="col-md-12 text-left"><b>Anuncie hoje o seu imóvel no Troca Imóvel!</b></div>
</div>
</div>
</div>

<div class="pagina-interna">
<div class="container">
<div class="row">
<div class="col-md-3 hidden-xs">

<?php include("sidebar.pesquisa.php"); ?>

</div>
    
<div class="col-md-9">
<div class="row">
<div class="col-md-12"><b class="text-primary"><i class="fa fa-file-o fa-fw" aria-hidden="true"></i> Como Funciona</b><hr class="hr-titulo"></div>
<div class="col-md-12 text-justify">

<b>SISTEMA EXCLUSIVO DE TROCA. QUE VAI FACILITAR O SEU NEGÓCIO.</b>
<p></p>
Através de um mecanismo entre os usuários cadastrados no site, você poderá pesquisar qualquer tipo de imóvel que tenham interesse em trocar pelo seu próprio imóvel. Seja um apartamento de três quartos em São Paulo por um casa de praia em Maresias, um tereno por outro em estados diferentes, entre todas outras infinitas possibilidades. 
E ao listar o que procura, cada imóvel terá uma porcentagem de combinação do seu interesse com o do outro usuário.
E isso será possível através da ferramenta exclusiva que criamos, onde você poderá pequisar e, ao se interessar, poderá curtir o imóvel que tem interesse. Se o dono deste imóvel o qual você se interessou também curtir o seu imóvel, vocês entrarão em combinação e serão avisados por notificação no aplicativo do celular e por e-mail. E automaticamente esse alerta criará um chat entre os dois para que comecem a negociação direta e sem complicação.

</div>
</div>
</div>
</div>
</div>

<?php include("footer.php"); ?>

<script src="<?php print($murl); ?>/js/bootstrap.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.money.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/script.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/pesquisa.min.js?<?php print($cache); ?>"></script>

</body>
</html>