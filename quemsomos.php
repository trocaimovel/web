<!DOCTYPE html>
<html lang="en">
<head>

<?php include("head.php"); ?>

<?php include("metatags.home.php") ?>

</head>
<body>
  
<?php include_once("analyticstracking.php") ?>

<?php include("navbar.php"); ?>

<div class="barra-titulo">
<div class="container">
<div class="row">
  <div class="col-md-12 text-left"><b>Anuncie hoje o seu imóvel no Troca Imóvel!</b></div>
</div>
</div>
</div>

<div class="pagina-interna">
<div class="container">
<div class="row">
<div class="col-md-3 hidden-xs">

<?php include("sidebar.pesquisa.php"); ?>

</div>
    
<div class="col-md-9">
<div class="row">
<div class="col-md-12"><b class="text-primary"><i class="fa fa-file-o fa-fw" aria-hidden="true"></i> Troca Imóvel</b><hr class="hr-titulo"></div>
<div class="col-md-12 text-justify">

"Originalmente, o termo mercado era utilizado para designar o sítio onde compradores e vendedores se encontravam para trocar os seus bens."
Hoje em dia, vemos muitos sites por aí que vendem ou alugam imóveis. Porém, eles não preenchem uma lacuna que ficou, até mesmo, um pouco esquecida no mercado atual: a troca.
Pensando nisso, aliamos a tecnologia do futuro com o mercado da maneira que era feito antigamente. E é por isso que o Troca Imóvel chega para fazer o diferente, para fazer o inédito em meio a tantos sites iguais do ramo imobiliário.
<p></p>

<div class="infografico">
  <img src="http://trocaimovel.com.br/img/infografico.png" class="img-infografico">
</div>

</div>
</div>
</div>
</div>
</div>

<?php include("footer.php"); ?>

<script src="<?php print($murl); ?>/js/bootstrap.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.money.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/script.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/pesquisa.min.js?<?php print($cache); ?>"></script>

</body>
</html>