<!DOCTYPE html>
<html lang="en">
<head>

<?php include("head.php"); ?>

<?php include("metatags.home.php") ?>

</head>
<body>
  
<?php include_once("analyticstracking.php") ?>

<?php include("navbar.php"); ?>

<div class="barra-titulo">
<div class="container">
<div class="row">
  <div class="col-md-12 text-left"><b>Anuncie hoje o seu imóvel no Troca Imóvel!</b></div>
</div>
</div>
</div>

<div class="pagina-interna">
<div class="container">
  <div class="row">
    <div class="col-md-3 hidden-xs">
      <?php include("sidebar.pesquisa.php"); ?>
    </div>
    <div class="col-md-9">
    <form id="formulario-cadastro">
      <div class="row">
        <div class="col-md-12"><b class="text-primary"><i class="fa fa-file-o fa-fw" aria-hidden="true"></i> Criar Conta</b><hr class="hr-titulo"></div>
        <div class="col-md-4">
          <label>Nome:</label>
          <input type="text" class="form-control text-capitalize" id="nome" name="nome" maxlength="20">
        </div>
        <div class="col-md-4">
          <label>Último sobrenome:</label>
          <input type="text" class="form-control text-capitalize" id="sobrenome" name="sobrenome" maxlength="20">
        </div>
        <div class="col-md-4">
          <label>Data de nascimento:</label>
          <input type="text" class="form-control" id="nascimento" name="nascimento">
        </div>
        <div class="col-md-3 m-top-10">
          <label>Sexo:</label>
          <select class="form-control" id="sexo" name="sexo">
            <option value="M">masculino</option>
            <option value="F">feminino</option>
          </select>
        </div>
        <div class="col-md-3 m-top-10">
          <label>Estado:</label>
          <select class="form-control" id="select-estado" name="idEstado" onchange="carregar_select_cidade('#formulario-cadastro ', $(this).val());"></select>
        </div>
        <div class="col-md-6 m-top-10">
          <label>Cidade:</label>
          <select class="form-control" id="select-cidade" name="idCidade"></select>
        </div>
        <div class="col-md-3 m-top-10">
          <label>RG:</label>
          <input type="text" class="form-control" id="rg" name="rg" placeholder="somente números" maxlength="15">
        </div>
        <div class="col-md-3 m-top-10">
          <label>CPF:</label>
          <input type="text" class="form-control" id="cpf" name="cpf" maxlength="14">
        </div>
        <div class="col-md-6 m-top-10">
          <label>E-mail de cadastro:</label>
          <input type="text" class="form-control text-lowercase" id="email-cadastro" name="emailCadastro" maxlength="50">
        </div>
        <div class="col-md-6 m-top-10">
          <label>E-mail para contato:</label>
          <input type="text" class="form-control text-lowercase" id="email-contato" name="emailContato" maxlength="50">
        </div>
        <div class="col-md-3 m-top-10">
          <label>Telefone para contato:</label>
          <input type="text" class="form-control" id="telefone" name="telefone" pattern="\([0-9]{2}\)[\s][0-9]{4}-[0-9]{4,5}">
        </div>
        <div class="col-md-3 m-top-10">
          <label>Exibir telefone no anúncio:</label>
          <select class="form-control" id="exibir-telefone" name="exibirTelefone">
            <option value="1">sim</option>
            <option value="0">não</option>
          </select>
        </div>
        <div class="col-md-3 m-top-10">
          <label>Senha:</label>
          <input type="password" class="form-control" id="senha" name="senha" maxlength="20">
        </div>
        <div class="col-md-3 m-top-10">
          <label>Repetir senha:</label>
          <input type="password" class="form-control" id="repetir-senha" name="repetirSenha" maxlength="20">
        </div>
        <div class="col-md-12">
          <hr>
          <div class="alert alert-success display-none" id="alert-success"></div>
          <div class="alert alert-danger display-none" id="alert-danger"></div>
          <div class="alert alert-warning display-none" id="alert-warning"></div>
          <button type="button" class="btn btn-danger btn-lg" onclick="criar_conta();" id="btn-submit"><i class="fa fa-magic" aria-hidden="true"></i> Criar Conta</button>
        </div>
      </div>
    </form>
    </div>
  </div>
</div>
</div>

<?php include("footer.php"); ?>

<script src="<?php print($murl); ?>/js/bootstrap.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/script.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/pesquisa.min.js?<?php print($cache); ?>"></script>
<script>
carregar_select_estado('#formulario-cadastro ');
carregar_select_cidade('#formulario-cadastro ');
carregar_select_bairro('#formulario-cadastro ');
</script>

</body>
</html>