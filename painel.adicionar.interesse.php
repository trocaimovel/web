<!DOCTYPE html>
<html lang="en">
<head>

<?php include("head.php"); ?>
<script>verificar_autenticacao(1);</script>

</head>
<body>

<?php include("navbar.php"); ?>

<?php include("conectado.php"); ?>

<div class="pagina-interna m-bottom-30">
<div class="container">
  <div class="row">
    <div class="col-md-3 hidden-sm hidden-xs">
      <?php include("sidebar.usuario.php"); ?>
    </div>
    <div class="col-md-9">
      <div class="row">
        <div class="col-md-12"><b class="text-primary"><i class="fa fa-file-o fa-fw" aria-hidden="true"></i> Adicionar Interesse</b><hr class="hr-titulo"></div>
        <div class="col-md-6">
          <label>Dê um título ao seu interesse:</label>
          <input type="text" class="form-control" id="titulo" name="titulo" maxlength="150">
        </div>
        <div class="col-md-3">
          <p class="visible-xs visible-sm"></p>
          <label>Categoria:</label>
          <select class="form-control" id="select-categoria" name="categoria"></select>
        </div>
        <div class="col-md-3">
          <p class="visible-xs visible-sm"></p>
          <label>Proposta:</label>
          <select class="form-control" id="select-proposta" name="proposta"></select>
        </div>
        <div class="col-md-4 m-top-10">
          <label>Estado:</label>
          <select class="form-control" id="select-estado" name="idEstado" onchange="carregar_select_cidade(null, $(this).val());"></select>
        </div>
        <div class="col-md-4 m-top-10">
          <label>Cidade:</label>
          <select class="form-control" id="select-cidade" name="idCidade" onchange="carregar_select_bairro(null, $(this).val());"></select>
        </div>
        <div class="col-md-4 m-top-10">
          <label>Bairro:</label>
          <select class="form-control" id="select-bairro" name="idBairro"></select>
        </div>
        <div class="col-md-3 m-top-10">
          <label>Nº Quartos:</label>
          <select class="form-control" id="n-quartos" name="nQuartos">
            <option value="0"></option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5+</option>
          </select>
        </div>
        <div class="col-md-3 m-top-10">
          <label>Nº Suítes:</label>
          <select class="form-control" id="n-suites" name="nSuites">
            <option value="0"></option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5+</option>
          </select>
        </div>
        <div class="col-md-3 m-top-10">
          <label>Nº Banheiros:</label>
          <select class="form-control" id="n-banheiros" name="nBanheiros">
            <option value="0"></option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5+</option>
          </select>
        </div>
        <div class="col-md-3 m-top-10">
          <label>Nº Vagas:</label>
          <select class="form-control" id="n-vagas" name="nVagas">
            <option value="0"></option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5+</option>
          </select>
        </div>
        <div class="col-md-3 m-top-10">
          <label>Área mínima:</label>
          <input type="number" class="form-control" id="metragem-minima" name="metragemMinima" min="0" max="99999" placeholder="em metros quadrados">
        </div>
        <div class="col-md-3 m-top-10">
          <label>Área máxima:</label>
          <input type="number" class="form-control" id="metragem-maxima" name="metragemMaxima" min="0" max="99999" placeholder="em metros quadrados">
        </div>
        <div class="col-md-3 m-top-10">
          <label>Valor mínimo:</label>
          <input type="text" class="form-control" id="valor-minimo" name="valorMinimo">
        </div>
        <div class="col-md-3 m-top-10">
          <label>Valor máximo:</label>
          <input type="text" class="form-control" id="valor-maximo" name="valorMaximo">
        </div>
        <div class="col-md-12">
          <hr>
          <div class="alert alert-success display-none" id="alert-success"></div>
          <div class="alert alert-danger display-none" id="alert-danger"></div>
          <div class="alert alert-warning display-none" id="alert-warning"></div>
          <button class="btn btn-danger btn-lg" onclick="adicionar_interesse();"><i class="fa fa-plus-square fa-fw" aria-hidden="true"></i> Adicionar Interesse</button>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<script src="<?php print($murl); ?>/js/bootstrap.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/bootstrap.filestyle.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.money.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/script.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/painel.min.js?<?php print($cache); ?>"></script>
<script>
carregar_select_estado();
carregar_select_cidade();
carregar_select_bairro();
carregar_select_categoria();
carregar_select_proposta();

$('#n-quartos').keypress(verificar_digito);
$('#n-suites').keypress(verificar_digito);
$('#n-banheiros').keypress(verificar_digito);
$('#n-vagas').keypress(verificar_digito);
$('#metragem-maxima').keypress(verificar_digito);
$('#metragem-minima').keypress(verificar_digito);

$('#valor-maximo').maskMoney({showSymbol:true, symbol:"R$", decimal:",", thousands:"."});
$('#valor-minimo').maskMoney({showSymbol:true, symbol:"R$", decimal:",", thousands:"."});
</script>

</body>
</html>