<?php
include("seguranca.php");
include("../lib/lib.imagem.php");

$nomeImagem = $_FILES["upload"]["name"];
$tamanhoImagem = $_FILES["upload"]["size"];
$temporarioImagem = $_FILES["upload"]["tmp_name"];
$tipoImagem = exif_imagetype($_FILES["upload"]["tmp_name"]);

$link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
if(!$link) die("Não foi possível conectar: ".mysql_error());

if($tipoImagem != IMAGETYPE_JPEG && $tipoImagem != IMAGETYPE_PNG){
  echo('[{"codigo":"1", "alerta":"A imagem não é um arquivo no formato jpeg ou png."}]');
}
else{
  $imagemToken = gera_token();

  if($tipoImagem == IMAGETYPE_JPEG) $extencao = ".jpg";
  elseif($tipoImagem == IMAGETYPE_PNG) $extencao = ".png";
  else{
    echo('[{"codigo":"2", "alerta":"A imagem não é um arquivo no formato jpeg ou png."}]');
    exit();
  }
  
  $arquivo = $imagemToken."-logo".$extencao;
  $resposta = move_uploaded_file($_FILES["upload"]["tmp_name"], "../files/$imagemToken"."-logo".$extencao);
  if($resposta == true) mysqli_query($link, "CALL sp_alterar_logotipo('$idUsuario','$arquivo')");
  echo('[{"codigo":"100", "alerta":"Logotipo alterado com sucesso."}]');
}
?>