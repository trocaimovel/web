<?php
require_once('PushBots.class.php');
include("seguranca.php");
include("../lib/lib.validacao.php");
include("../lib/lib.anuncio.php");
include("../lib/lib.email.php");

$link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
if(!$link) die("Não foi possível conectar: ".mysql_error());

$idUsuarioInteressado = $idUsuario;
$idAnuncioInteressado = (int)$_POST["idAnuncioInteressado"];
$idAnuncioProprietario = (int)$_POST["idAnuncioProprietario"];
$idUsuarioAnunciante = valida_usuario_anunciante($idAnuncioInteressado);

if($idUsuarioInteressado == $idUsuarioAnunciante){
  echo('[{"codigo":"1", "alerta":"Você é proprietário deste imóvel."}]');
  exit();
}
elseif(valida_desejo_anuncio($idUsuarioInteressado, $idAnuncioInteressado, $idAnuncioProprietario) == false){
  echo('[{"codigo":"2", "alerta":"Este imóvel está na sua lista de desejos."}]');
  exit();
}
elseif($idAnuncioInteressado == false){
  echo('[{"codigo":"3", "alerta":"O usuário que está anunciando este imóvel não existe."}]');
  exit();
}
else{
  $resposta = mysqli_query($link, utf8_decode("CALL sp_adicionar_desejo('$idUsuarioInteressado','$idAnuncioProprietario','$idAnuncioInteressado','$idUsuarioAnunciante')"));
  if($resposta == true){
    if(valida_combinacao($idUsuarioInteressado, $idAnuncioInteressado, $idAnuncioProprietario, $idUsuarioAnunciante) == true){
      echo('[{"codigo":"4", "alerta":"Parabéns! Ocorreu uma combinação, este anunciante também está interessado no seu imóvel, converse com ele agora mesmo."}]');
      
      $mensagemIdAnuncioProprietario = str_pad((string)$idAnuncioProprietario, 5, "0", STR_PAD_LEFT);
      $mensagemTituloAnuncioProprietario = retorna_titulo_anuncio($idAnuncioProprietario);
      
      $mensagemIdAnuncioInteressado = str_pad((string)$idAnuncioInteressado, 5, "0", STR_PAD_LEFT);
      $mensagemTituloAnuncioInteressado = retorna_titulo_anuncio($idAnuncioInteressado);
      
      $mensagemChat = 'Olá! Estou interessado no seu anúncio número <a class="text-danger" href="http://trocaimovel.com.br/anuncio/'.$mensagemIdAnuncioInteressado.'">'.$mensagemIdAnuncioInteressado.'</a>
      e você no meu <a class="text-danger" href="http://trocaimovel.com.br/anuncio/'.$mensagemIdAnuncioProprietario.'">'.$mensagemIdAnuncioProprietario.'</a>, vamos negociar.';
      
      mysqli_query($link, utf8_decode("CALL sp_enviar_mensagem('$idUsuarioInteressado','$idUsuarioAnunciante','$mensagemChat')"));
      
      $emailInteressado = retorna_email_anunciante($idUsuarioInteressado);
      $emailAnunciante = retorna_email_anunciante($idUsuarioAnunciante);
      
      $mensagemEmail ='Parabéns! Você tem uma nova combinação no Troca Imóvel.<br>
      Alguém está interessado em negociar um imóvel com você.<p></p>
      Acesse sua conta no Troca Imóvel e confira no menu Combinações.<p></p>
      
      Equipe Troca Imóvel,<br>
      <a href="http://trocaimovel.com.br">http://trocaimovel.com.br</a><br>
      contato@trocaimovel.com.br';
      enviar_email("Troca Imóvel: Nova Combinação", $emailAnunciante, $mensagemEmail);
      enviar_email("Troca Imóvel: Nova Combinação", $emailInteressado, $mensagemEmail);
    }
    else{
      echo('[{"codigo":"100", "alerta":"Este imóvel foi adicionado a sua lista de desejos, caso o anunciante também se interesse no seu, vocês serão notificados."}]');
    }
  }
  else echo('[{"codigo":"200", "alerta":"Erro ao conectar com banco de dados."}]');
  mysqli_close($link);
}
?>