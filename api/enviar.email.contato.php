<?php
header("Access-Control-Allow-Origin: *");
include("../lib/lib.validacao.php");
include("../lib/lib.email.php");
include("../lib/lib.anuncio.php");

$idAnuncio = (int)$_POST["idAnuncio"];
$anuncio = retorna_titulo_anuncio($idAnuncio);
$emailDestinatario = retorna_email_anunciante($idAnuncio);
$remetente = ucfirst(strtolower(strip_tags(addslashes($_POST["contatoNome"]))));
$emailRemetente = strtolower(strip_tags(addslashes($_POST["contatoEmail"])));
$telefoneRemetente = strip_tags(addslashes($_POST["contatoTelefone"]));
$mensagem = strip_tags(addslashes($_POST["contatoMensagem"]));

if($idAnuncio == 0){
  echo('[{"codigo":"1", "alerta":"O anúncio não existe."}]');
}
elseif(strlen($remetente) < 3){
  echo('[{"codigo":"2", "alerta":"Nome inválido."}]');
}
elseif(valida_email($emailRemetente) == false){
  echo('[{"codigo":"3", "alerta":"E-mail inválido."}]');
}
elseif(strlen($telefoneRemetente) < 8){
  echo('[{"codigo":"4", "alerta":"Telefone inválido."}]');
}
elseif(strlen($mensagem) < 10){
  echo('[{"codigo":"5", "alerta":"Mensagem muito pequena."}]');
}
else{
  $mensagemEmail ='Parece que alguém está interessado em um dos seus anúncios no Troca Imóvel.<p></p>
      
  Nome: '.$remetente.'<br>
  E-mail para resposta: '.$emailRemetente.'<br>
  Anúncio: '.$anuncio.'<p></p>
  Mensagem:<p></p>'.$mensagem.'<p></p>
      
  Equipe Troca Imóvel,<br>
  <a href="http://trocaimovel.com.br">http://trocaimovel.com.br</a><br>
  contato@trocaimovel.com.br';
  
  $email = enviar_email("Troca Imóvel: Novo Contato", $emailDestinatario, $mensagemEmail);
  if($email == true){
    echo('[{"codigo":"100", "alerta":"Mensagem enviada com sucesso."}]');
  }
  else{
    echo('[{"codigo":"200", "alerta":"Servidor de e-mail indisponível."}]');
  }
}
?>