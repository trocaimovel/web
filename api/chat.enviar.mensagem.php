<?php
include("seguranca.php");

$idDestinatario = (int)$_POST["idDestinatario"];
$mensagem = addslashes(strip_tags($_POST["mensagem"]));

$link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
if(!$link) die("Não foi possível conectar: ".mysql_error());

if($mensagem == ""){
  echo('[{"codigo":"1", "alerta":"Escreva uma mensagem para enviar."}]');
}
elseif($idDestinatario == 0){
  echo('[{"codigo":"2", "alerta":"Destinatário inválido."}]');
}
elseif($idDestinatario == $idUsuario){
  echo('[{"codigo":"3", "alerta":"Destinatário inválido."}]');
}
else{
  $resposta = mysqli_query($link, utf8_decode("CALL sp_chat_enviar_mensagem('$idUsuario','$idDestinatario','$mensagem')"));
  if($resposta == true) echo('[{"codigo":"100", "alerta":"Mensagem enviada com sucesso!"}]');
  else echo('[{"codigo":"200", "alerta":"Erro ao conectar com banco de dados."}]');
  mysqli_close($link);
}
?>