<?php
include("seguranca.php");
include("../lib/lib.validacao.php");
include("../lib/lib.mysql.php");
include("../lib/lib.moeda.php");
require("wideimage/WideImage.php");
include("../lib/lib.imagem.php");
include("../lib/lib.youtube.php");

$idAnuncio = (int)$_POST["idAnuncio"];
$titulo = addslashes(strip_tags($_POST["titulo"]));
$descricao = addslashes(strip_tags($_POST["descricao"]));
$idCategoria = (int)$_POST["idCategoria"];
$idProposta = (int)$_POST["idProposta"];
$idEstado = (int)$_POST["idEstado"];
$idCidade = (int)$_POST["idCidade"];
$idBairro = (int)$_POST["idBairro"];
$nQuartos = (int)$_POST["nQuartos"];
$nSuites = (int)$_POST["nSuites"];
$nBanheiros = (int)$_POST["nBanheiros"];
$nVagas = (int)$_POST["nVagas"];
$metragem = (int)$_POST["metragem"];
$valorImovel = formatar_valor_moeda($_POST["valorImovel"]);
$valorCondominio = formatar_valor_moeda($_POST["valorCondominio"]);
$valorIPTU = formatar_valor_moeda($_POST["valorIPTU"]);
$videoYoutube = addslashes(strip_tags($_POST["videoYoutube"]));

$link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
if(!$link) die("Não foi possível conectar: ".mysql_error());

if(strlen($titulo) < 10){
  echo('[{"codigo":"1", "alerta":"O título é muito pequeno."}]');
}
elseif(strlen($descricao) < 10){
  echo('[{"codigo":"2", "alerta":"A descrição é muito pequena."}]');  
}
elseif($idCategoria == 0){
  echo('[{"codigo":"3", "alerta":"Selecione uma categoria."}]');
}
elseif($idProposta == 0){
  echo('[{"codigo":"4", "alerta":"Selecione a proposta."}]');
}
elseif($idEstado == 0){
  echo('[{"codigo":"5", "alerta":"Selecione o Estado."}]');
}
elseif($idCidade == 0){
  echo('[{"codigo":"6", "alerta":"Selecione a cidade."}]');
}
elseif($idBairro == 0){
  echo('[{"codigo":"7", "alerta":"Selecione o bairro."}]');
}
elseif((retorna_youtube_video_id($videoYoutube) == false) && (strlen($videoYoutube) > 0)){
  echo('[{"codigo":"8", "alerta":"A URL informada para o vídeo no Youtube é inválida."}]');
}
else{
  if((retorna_youtube_video_id($videoYoutube) != false) && (strlen($videoYoutube) > 0)) $videoYoutube = retorna_youtube_video_id($videoYoutube);
  $resposta = mysqli_query($link, utf8_decode("CALL sp_editar_anuncio('$idUsuario','$idAnuncio','$titulo','$descricao','$idCategoria','$idProposta','$idEstado','$idCidade','$idBairro','$nQuartos','$nSuites','$nBanheiros','$nVagas','$metragem','$valorImovel','$valorCondominio','$valorIPTU','$videoYoutube')"));
  if($resposta == true){
    if(count($_FILES["upload"]["name"]) > 0){
      for($i = 0; $i < count($_FILES["upload"]["name"]); $i++){  
        
        $nomeImagem = $_FILES["upload"]["name"][$i];
        $tamanhoImagem = $_FILES["upload"]["size"][$i];
        $temporarioImagem = $_FILES["upload"]["tmp_name"][$i];
        
        if($temporarioImagem != ""){
          $tiposPermitidos = array(IMAGETYPE_JPEG);
          $tipoImagem = exif_imagetype($_FILES["upload"]["tmp_name"][$i]);
          if($tipoImagem != IMAGETYPE_JPEG) echo('[{"codigo":"1", "alerta":"A foto enviada na Imagem '.$i.' não é um arquivo no formato jpeg."}]');
          else{
            $arquivo = gera_token();
            $resposta = move_uploaded_file($_FILES["upload"]["tmp_name"][$i], "../files/$arquivo".".jpg");
            if($resposta == true) imagem_redimensionar("../files", $arquivo, "jpg", 800, 600);
            if($resposta == true) imagem_gerar_thumb("../files", $arquivo, "jpg", 200, 150);
            if($resposta == true) mysqli_query($link, "CALL sp_inserir_imagem_anuncio('$idAnuncio','$idUsuario','http://trocaimovel.com.br/files/$arquivo.jpg')");
          }
        }
      }
    }
    echo('[{"codigo":"100", "alerta":"Dados alterados com sucesso!"}]');
  }
  else echo('[{"codigo":"200", "alerta":"Erro ao conectar com banco de dados."}]');
  mysqli_close($link);
}
?>