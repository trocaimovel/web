<?php
include("seguranca.php");
include("../lib/lib.validacao.php");

$senhaAtual = md5("troca".strip_tags(strtolower(addslashes($_POST["senhaAtual"]))));
$novaSenha = strip_tags(strtolower(addslashes($_POST["novaSenha"])));
$repetirSenha = strip_tags(strtolower(addslashes($_POST["repetirSenha"])));

if(valida_senha_usuario($senhaAtual, $idUsuario) == false){
  echo('[{"codigo":"1", "alerta":"Senha atual inválida."}]');  
}
elseif($novaSenha != $repetirSenha){
  echo('[{"codigo":"2", "alerta":"As senhas não conferem."}]');
}
elseif(strlen($novaSenha) < 5){
  echo('[{"codigo":"3", "alerta":"A senha é muito pequena."}]');
}
else{
  $novaSenha = md5("troca".$novaSenha);
  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  if(!$link) die("Não foi possível conectar: ".mysql_error());
  $resposta = mysqli_query($link, utf8_decode("CALL sp_editar_senha_usuario('$idUsuario','$senhaAtual','$novaSenha')"));
  if($resposta == true) echo('[{"codigo":"100", "alerta":"Senha alterada com sucesso."}]');
  else echo('[{"codigo":"200", "alerta":"Erro ao conectar com banco de dados."}]');
  mysqli_close($link);
}
?>