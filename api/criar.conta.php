<?php
header("Access-Control-Allow-Origin: *");
include("../database.php");
include("../lib/lib.validacao.php");
include("../lib/lib.token.php");
include("../lib/lib.mysql.php");
include("../lib/lib.data.php");
include("../lib/lib.email.php");

$nome = addslashes(strip_tags(ucwords(strtolower($_POST["nome"]))));
$sobrenome = addslashes(strip_tags(ucwords(strtolower($_POST["sobrenome"]))));
@$nascimento = strip_tags($_POST["nascimento"]);
$sexo = strip_tags($_POST["sexo"]);
$idEstado = (int)$_POST["idEstado"];
$idCidade = (int)$_POST["idCidade"];
@$cpf = strip_tags($_POST["cpf"]);
@$rg = strip_tags($_POST["rg"]);
$emailCadastro = strip_tags(strtolower($_POST["emailCadastro"]));
$emailContato = strip_tags(strtolower($_POST["emailContato"]));
$telefone = strip_tags($_POST["telefone"]);
$exibirTelefone = (int)$_POST["exibirTelefone"];
$senha = addslashes(strip_tags(strtolower($_POST["senha"])));
$repetirSenha = addslashes(strip_tags(strtolower($_POST["repetirSenha"])));
$nomeCompleto = $nome." ".$sobrenome;

if(valida_nome($nome) == false){
  echo('[{"codigo":"1", "alerta":"Nome inválido."}]');
}
elseif(valida_nome($sobrenome) == false){
  echo('[{"codigo":"2", "alerta":"Sobrenome inválido."}]');
}
elseif((valida_data_nascimento($nascimento) == false) && ($nascimento != "00/00/0000")){
  echo('[{"codigo":"3", "alerta":"Data de nascimento inválida."}]');
}
elseif(valida_sexo($sexo) == false){
  echo('[{"codigo":"4", "alerta":"Selecione o seu sexo."}]');
}
elseif($idEstado == 0){
  echo('[{"codigo":"5", "alerta":"Informe o Estado."}]');
}
elseif($idCidade == 0){
  echo('[{"codigo":"6", "alerta":"Informe a cidade."}]');
}
elseif((valida_rg($rg, null, 0) == false) && (strlen($rg) > 0)){
  echo('[{"codigo":"7", "alerta":"RG inválido ou em uso."}]');
}
elseif((valida_cpf($cpf, null, 0) == false) && (strlen($cpf) > 0)){
  echo('[{"codigo":"8", "alerta":"CPF inválido ou em uso."}]');
}
elseif(valida_email_cadastro($emailCadastro, null, 0) == false){
  echo('[{"codigo":"9", "alerta":"E-mail de cadastro inválido ou em uso."}]');
}
elseif(valida_email_contato($emailContato, null, 0) == false){
  echo('[{"codigo":"10", "alerta":"O e-mail de contato inválido ou em uso."}]');
}
elseif(strlen($telefone) != 0 && strlen($telefone) < 8){
  echo('[{"codigo":"11", "alerta":"Telefone inválido."}]');
}
elseif($exibirTelefone > 1){
  echo('[{"codigo":"12", "alerta":"Selecione se você deseja exibir ou não o seu telefone."}]');
}
elseif($senha != $repetirSenha){
  echo('[{"codigo":"13", "alerta":"As senhas não conferem."}]');
}
elseif(strlen($senha) < 5){
  echo('[{"codigo":"14", "alerta":"A senha é muito pequena."}]');
}
else{
  $nascimento = converte_data_mysql($nascimento);
  $senhaEmail = $senha;
  $senha = md5("troca".$senha);
  
  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  if(!$link) die("Não foi possível conectar: ".mysql_error());
  $resposta = mysqli_query($link, utf8_decode("CALL sp_criar_conta('0','0','$nome','$sobrenome','$nascimento','$sexo','$idEstado','$idCidade','$cpf','$rg','$emailCadastro','$emailContato','$telefone','1','$exibirTelefone','$senha','')"));
  if($resposta == true){
    $token = gera_token();
    $ip = $_SERVER['REMOTE_ADDR'];
    $idUsuario = retorna_ultimo_id("usuario");
    $resposta = mysqli_query($link, "CALL sp_registrar_acesso('$idUsuario','$ip','$token')");
    echo('[{"codigo":"100", "alerta":"Usuário cadastrado com sucesso.", "token":"'.$token.'"}]');
    
    $mensagem ='Sua conta no Troca Imóvel foi criada com sucesso.<p></p>
    
    Nome: '.$nomeCompleto.'<br>
    E-mail da cadastro: '.$emailCadastro.'<br>
    Senha: '.$senhaEmail.'<p></p>
    
    Equipe Troca Imóvel,<br>
    <a href="http://trocaimovel.com.br">http://trocaimovel.com.br</a><br>
    contato@trocaimovel.com.br';
    enviar_email("Troca Imóvel: Conta Criada", $emailCadastro, $mensagem);
  }
  else{
    echo('[{"codigo":"200", "alerta":"Erro ao conectar com banco de dados."}]');    
  }
  mysqli_close($link);
}
?>