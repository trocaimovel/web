<?php
header("Access-Control-Allow-Origin: *");
include("../database.php");
include("../lib/lib.validacao.php");
include("../lib/lib.senha.php");
include("../lib/lib.email.php");

$emailCadastro = strip_tags(strtolower(addslashes($_POST["emailCadastro"])));
$cpf = strip_tags(addslashes($_POST["cpf"]));

if(valida_cpf_usuario($cpf, $emailCadastro) == false){
  echo('[{"codigo":"1", "alerta":"O CPF informado não está associado a este e-mail de cadastro."}]');  
}
else{
  $senhaGerada = gerar_senha(10);
  $novaSenha = md5("troca".$senhaGerada);
  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  if(!$link) die("Não foi possível conectar: ".mysql_error());
  $resposta = mysqli_query($link, utf8_decode("CALL sp_nova_senha_usuario('$emailCadastro','$cpf','$novaSenha')"));
  if($resposta == true){
    echo('[{"codigo":"100", "alerta":"Uma nova senha foi enviada para o seu e-mail de cadastro."}]');
    
    $mensagem ='Sua nova senha no Troca Imóvel é: '.$senhaGerada.'<p></p>
    Equipe Troca Imóvel,<br>
    <a href="http://trocaimovel.com.br">http://trocaimovel.com.br</a><br>
    contato@trocaimovel.com.br';
    enviar_email("Troca Imóvel: Nova Senha", $emailCadastro, $mensagem);
  }
  else echo('[{"codigo":"200", "alerta":"Erro ao conectar com banco de dados."}]');
  mysqli_close($link);
}
?>