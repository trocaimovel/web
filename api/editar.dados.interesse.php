<?php
include("seguranca.php");
include("../lib/lib.validacao.php");
include("../lib/lib.mysql.php");
include("../lib/lib.moeda.php");

$idInteresse = (int)$_POST["idInteresse"];
$titulo = addslashes(strip_tags($_POST["titulo"]));
$idCategoria = (int)$_POST["idCategoria"];
$idProposta = (int)$_POST["idProposta"];
$idEstado = (int)$_POST["idEstado"];
$idCidade = (int)$_POST["idCidade"];
$idBairro = (int)$_POST["idBairro"];
$nQuartos = (int)$_POST["nQuartos"];
$nSuites = (int)$_POST["nSuites"];
$nBanheiros = (int)$_POST["nBanheiros"];
$nVagas = (int)$_POST["nVagas"];
$metragemMinima = (int)$_POST["metragemMinima"];
$metragemMaxima = (int)$_POST["metragemMaxima"];
$valorMinimo = formatar_valor_moeda($_POST["valorMinimo"]);
$valorMaximo = formatar_valor_moeda($_POST["valorMaximo"]);

$link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
if(!$link) die("Não foi possível conectar: ".mysql_error());

if($idInteresse == 0){
  echo('[{"codigo":"1", "alerta":"Interesse não cadastrado."}]');
}
elseif(strlen($titulo) < 10){
  echo('[{"codigo":"2", "alerta":"O título é muito pequeno."}]');
}
elseif($idCategoria == 0){
  echo('[{"codigo":"3", "alerta":"Selecione uma categoria."}]');
}
elseif($idProposta == 0){
  echo('[{"codigo":"4", "alerta":"Selecione qual o tipo de proposta."}]');
}
elseif($idEstado == 0){
  echo('[{"codigo":"5", "alerta":"Selecione o Estado."}]');
}
elseif($idCidade == 0){
  echo('[{"codigo":"6", "alerta":"Selecione a cidade."}]');
}
elseif($idBairro == 0){
  echo('[{"codigo":"7", "alerta":"Selecione o bairro."}]');
}
else{
  $resposta = mysqli_query($link, utf8_decode("CALL sp_editar_dados_interesse('$idInteresse','$idUsuario','$titulo','$idCategoria','$idProposta','$idEstado','$idCidade','$idBairro','$nQuartos','$nSuites','$nBanheiros','$nVagas','$metragemMaxima','$metragemMinima','$valorMaximo','$valorMinimo')"));
  if($resposta == true) echo('[{"codigo":"100", "alerta":"Dados alterados com sucesso!"}]');
  else echo('[{"codigo":"200", "alerta":"Erro ao conectar com banco de dados."}]');
  mysqli_close($link);
}
?>