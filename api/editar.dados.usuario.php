<?php
include("seguranca.php");
include("../lib/lib.validacao.php");
include("../lib/lib.mysql.php");
include("../lib/lib.data.php");

$nome = addslashes(strip_tags(ucwords(strtolower($_POST["nome"]))));
$sobrenome = addslashes(strip_tags(ucwords(strtolower($_POST["sobrenome"]))));
@$nascimento = strip_tags($_POST["nascimento"]);
$sexo = strip_tags($_POST["sexo"]);
$idEstado = (int)$_POST["idEstado"];
$idCidade = (int)$_POST["idCidade"];
@$cpf = strip_tags($_POST["cpf"]);
@$rg = strip_tags($_POST["rg"]);
$emailContato = strip_tags(strtolower($_POST["emailContato"]));
$telefone = strip_tags($_POST["telefone"]);
$exibirTelefone = (int)$_POST["exibirTelefone"];

if(valida_nome($nome) == false){
  echo('[{"codigo":"1", "alerta":"Nome inválido."}]');
}
elseif(valida_nome($sobrenome) == false){
  echo('[{"codigo":"2", "alerta":"Sobrenome inválido."}]');
}
elseif(valida_data_nascimento($nascimento) == false){
  echo('[{"codigo":"3", "alerta":"Data de nascimento inválida."}]');
}
elseif(valida_sexo($sexo) == false){
  echo('[{"codigo":"4", "alerta":"Selecione o seu sexo."}]');
}
elseif($idEstado == 0){
  echo('[{"codigo":"5", "alerta":"Informe o Estado."}]');
}
elseif($idCidade == 0){
  echo('[{"codigo":"6", "alerta":"Informe a cidade."}]');
}
elseif((valida_rg($rg, null, 0) == false) && (strlen($rg) > 0)){
  echo('[{"codigo":"7", "alerta":"RG inválido ou em uso."}]');
}
elseif((valida_cpf($cpf, null, 0) == false) && (strlen($cpf) > 0)){
  echo('[{"codigo":"8", "alerta":"CPF inválido ou em uso."}]');
}
elseif(valida_email_contato($emailContato, $idUsuario, 1) == false){
  echo('[{"codigo":"9", "alerta":"E-mail de contato inválido."}]');
}
elseif(strlen($telefone) != 0 && strlen($telefone) < 8){
  echo('[{"codigo":"10", "alerta":"Telefone inválido."}]');
}
elseif($exibirTelefone > 1){
  echo('[{"codigo":"11", "alerta":"Selecione se você deseja exibir ou não o seu telefone."}]');
}
else{
  $nascimento = converte_data_mysql($nascimento);

  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  if(!$link) die("Não foi possível conectar: ".mysql_error());
  $resposta = mysqli_query($link, utf8_decode("CALL sp_editar_dados_usuario('$idUsuario','$nome','$sobrenome','$nascimento','$sexo','$idEstado','$idCidade','$cpf','$rg','$emailContato','$telefone','1','$exibirTelefone')"));
  if($resposta == true) echo('[{"codigo":"100", "alerta":"Dados alterados com sucesso."}]');
  else echo('[{"codigo":"200", "alerta":"Erro ao conectar com banco de dados."}]');
  mysqli_close($link);
}
?>