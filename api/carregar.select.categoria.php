<?php
header("Access-Control-Allow-Origin: *");
include("../database.php");

$link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
if(!$link) die("Não foi possível conectar: ".mysql_error());

$query = mysqli_query($link, "SELECT * FROM tb_categoria_imovel WHERE 1 ORDER BY categoria");

if(sizeof($query) > 0){
	while($col = mysqli_fetch_assoc($query)){
		foreach(array_keys($col) as $key) $col[$key] = iconv("iso-8859-1","utf-8", $col[$key]);
		$data[] = $col;
	}
}

if(isset($data)) echo json_encode($data);
else echo("null");
mysqli_close($link);
?>