<?php
include("seguranca.php");
include("../lib/lib.validacao.php");

$senha = md5("troca".strip_tags(strtolower(addslashes($_POST["senha"]))));
$confirmacao = (int)$_POST["confirmacao"];

if(valida_senha_usuario($senha, $idUsuario) == false){
  echo('[{"codigo":"1", "alerta":"A senha atual não está correta."}]');  
}
elseif($confirmacao == false){
  echo('[{"codigo":"2", "alerta":"Você deve confirmar a remoção da sua conta."}]');
}
else{
  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  if(!$link) die("Não foi possível conectar: ".mysql_error());
  $resposta = mysqli_query($link, utf8_decode("CALL sp_remover_conta_usuario('$idUsuario','$senha')"));
  if($resposta == true){
    $resposta = mysqli_query($link, "CALL sp_remover_anuncios_usuario('$idUsuario')");
    echo('[{"codigo":"100", "alerta":"Conta removida com sucesso."}]');
  }
  else echo('[{"codigo":"200", "alerta":"Erro ao conectar com banco de dados."}]');
  mysqli_close($link);
}
?>