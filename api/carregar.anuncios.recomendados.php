<?php
include("seguranca.php");

$link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
if(!$link) die("Não foi possível conectar: ".mysql_error());

$idUsuario = retorna_id_usuario($token);

$queryInteresse = mysqli_query($link, "SELECT id_interesse, id_categoria_imovel, id_proposta_imovel, id_bairro, numero_quartos, numero_vagas, valor_maximo FROM tb_interesse WHERE id_usuario = '$idUsuario' AND situacao = '1'");
while($colInteresse = mysqli_fetch_assoc($queryInteresse)){
  $idCategoriaInteresse = $colInteresse["id_categoria_imovel"];
  $idPropostaInteresse = $colInteresse["id_proposta_imovel"];
  $idBairroInteresse = $colInteresse["id_bairro"];
  $numeroQuartosInteresse = $colInteresse["numero_quartos"];
  $numeroVagasInteresse = $colInteresse["numero_vagas"];
  $valorMaximoInteresse = $colInteresse["valor_maximo"];
  
  $queryAnuncio = mysqli_query($link, "SELECT * FROM vw_anuncio WHERE id_categoria_imovel = '$idCategoriaInteresse' AND id_bairro = '$idBairroInteresse' AND id_usuario <> '$idUsuario' AND situacao = '1' ORDER BY RAND() LIMIT 10");
  while($colAnuncio = mysqli_fetch_assoc($queryAnuncio)){
    $idCategoriaAnuncio = $colAnuncio["id_categoria_imovel"];
    $idPropostaAnuncio = $colAnuncio["id_proposta_imovel"];
    $idBairroAnuncio = $colAnuncio["id_bairro"];
    $numeroQuartosAnuncio = $colAnuncio["numero_quartos"];
    $numeroVagasAnuncio = $colAnuncio["numero_vagas"];
    $valorAnuncio = $colAnuncio["valor_imovel"];
  
    $pontuacao = 40;
    if($idPropostaAnuncio == $idPropostaInteresse) $pontuacao += 15;
    if($numeroQuartosAnuncio == $numeroQuartosInteresse) $pontuacao += 15;
    if($numeroVagasAnuncio == $numeroQuartosInteresse) $pontuacao += 10;
    if($valorAnuncio <= $valorMaximoInteresse) $pontuacao += 10;
  
		foreach(array_keys($colAnuncio) as $key) $colAnuncio[$key] = iconv("iso-8859-1","utf-8", $colAnuncio[$key]);
    $colAnuncio = array_merge($colAnuncio, array("pontuacao" => $pontuacao));
		$data[] = $colAnuncio;
  }
}

if(isset($data)){
  $data = json_encode($data);
  $data = substr($data, 0, -2);
  $data .= ',"codigo":"100"}]';
  echo($data);
}
else echo("null");
mysqli_close($link);
?>