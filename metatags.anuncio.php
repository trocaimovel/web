<?php
include("database.php");
$link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
if(!$link) die("Não foi possível conectar: ".mysql_error());
$idAnuncio = explode("/", $_SERVER['REQUEST_URI']);
$idAnuncio = (int)$idAnuncio[2];

$query = mysqli_query($link, "SELECT * FROM vw_anuncio WHERE id_anuncio = '$idAnuncio' AND situacao = '1'");
if(sizeof($query) > 0){
  while($col = mysqli_fetch_assoc($query)){
    $categoria = $col["categoria"];
    $proposta = $col["proposta"];
    $cidade = $col["cidade"];
    $numeroQuartos = (int)$col["numero_quartos"];
  }
  $descricao = "Veja este anúncio no Troca Imóvel, ".$categoria." para ".$proposta." em ".$cidade;
  if($numeroQuartos > 0) $descricao = "Veja este anúncio no Troca Imóvel, ".$categoria." com ".$numeroQuartos." quartos para ".$proposta." em ".$cidade;
}
?>
<meta name="keywords" content="imóveis, casas, apartamentos, lojas, alugar, comprar, vender, trocar"/>
<meta name="description" content="Todas as formas de negociar o seu imóvel, em um só aplicativo.">
<meta name="subject" content="<?=$descricao?>">
<meta name="copyright" content="Troca Imóvel">
<meta name="language" content="pt-br">
<meta name="robots" content="index,follow">
<meta name="author" content="Troca Imóvel, contato@trocaimovel.com.br">
<meta name="reply-to" content="contato@trocaimovel.com.br">
<meta name="url" content="http://trocaimovel.com.br/anuncio/<?=$idAnuncio?>">
<meta name="identifier-URL" content="http://trocaimovel.com.br/anuncio/<?=$idAnuncio?>">
<meta http-equiv="Expires" content="0">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">

<meta name="og:title" content="<?=$descricao?>"/>
<meta name="og:url" content="http://trocaimovel.com.br/anuncio/<?=$idAnuncio?>"/>
<meta name="og:image" content="http://trocaimovel.com.br/img/og.png"/>
<meta name="og:site_name" content="Troca Imóvel"/>
<meta name="og:description" content="Todas as formas de negociar o seu imóvel, em um só aplicativo."/>