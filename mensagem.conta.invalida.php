<!DOCTYPE html>
<html lang="en">
<head>

<?php include("head.php"); ?>

</head>
<body>

<?php include("navbar.php"); ?>

<div class="barra-titulo">
<div class="container">
<div class="row">
  <div class="col-md-12 text-left"><b>Anuncie hoje o seu imóvel no Troca Imóvel!</b></div>
</div>
</div>
</div>

<div class="pagina-interna">
<div class="container">
<div class="row">
<div class="col-md-3 hidden-xs">

<?php include("sidebar.pesquisa.php"); ?>

</div>
    
<div class="col-md-9">
<div class="row">
<div class="col-md-12"><b class="text-primary"><i class="fa fa-file-o fa-fw" aria-hidden="true"></i> Conta Inválida</b><hr class="hr-titulo"></div>
<div class="col-md-12">
  <div class="alert alert-danger" id="mensagem-conta-criada">Você ainda não logou no Troca Imóvel ou a sessão expirou.</div>
  <a class="btn btn-default btn-lg" data-toggle="modal" data-target=".modal-login"><i class="fa fa-sign-in text-danger" aria-hidden="true"></i> Acessar Minha Conta</a>  
</div>
</div>
</div>
</div>
</div>

<?php include("footer.php"); ?>

<script src="<?php print($murl); ?>/js/bootstrap.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/script.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/pesquisa.min.js?<?php print($cache); ?>"></script>

</body>
</html>