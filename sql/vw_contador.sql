SELECT
(SELECT COUNT(*) FROM tb_anuncio WHERE 1 AND situacao = 1) AS 'total',
(SELECT COUNT(*) FROM tb_anuncio WHERE id_categoria_imovel = 2 AND situacao = 1) AS 'total_apartamentos',
(SELECT COUNT(*) FROM tb_anuncio WHERE id_categoria_imovel = 6 AND situacao = 1) AS 'total_casas',
(SELECT COUNT(*) FROM tb_anuncio WHERE id_categoria_imovel = 20 AND situacao = 1) AS 'total_flats',
(SELECT COUNT(*) FROM tb_anuncio WHERE id_categoria_imovel = 30 AND situacao = 1) AS 'total_loteamentos',
(SELECT COUNT(*) FROM tb_anuncio WHERE id_categoria_imovel = 29 AND situacao = 1) AS 'total_lojas',
(SELECT COUNT(*) FROM tb_anuncio WHERE id_categoria_imovel = 36 AND situacao = 1) AS 'total_salas'