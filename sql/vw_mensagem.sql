SELECT
tb_mensagem.id_mensagem,
tb_mensagem.id_remetente,
tb_mensagem.id_destinatario,
(SELECT nome FROM tb_usuario WHERE tb_usuario.id_usuario = tb_mensagem.id_remetente) AS 'nome_remetente',
(SELECT sobrenome FROM tb_usuario WHERE tb_usuario.id_usuario = tb_mensagem.id_remetente) AS 'sobrenome_remetente',
(SELECT nome FROM tb_usuario WHERE tb_usuario.id_usuario = tb_mensagem.id_destinatario) AS 'nome_destinatario',
(SELECT sobrenome FROM tb_usuario WHERE tb_usuario.id_usuario = tb_mensagem.id_destinatario) AS 'sobrenome_destinatario',
tb_mensagem.`data` AS 'data_sql',
tb_mensagem.hora AS 'hora_sql',
DATE_FORMAT(tb_mensagem.`data`, '%d/%m/%Y') AS 'data',
DATE_FORMAT(tb_mensagem.hora, '%H:%i') AS 'hora',
tb_mensagem.mensagem,
tb_mensagem.situacao_leitura,
tb_mensagem.situacao_remetente,
tb_mensagem.situacao_destinatario
FROM tb_mensagem WHERE 1