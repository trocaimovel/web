SELECT 
tb_anuncio.id_anuncio,
tb_anuncio.id_usuario,
tb_usuario.nome,
tb_usuario.sobrenome,
tb_anuncio.data_publicacao,
tb_anuncio.data_remocao,
tb_anuncio.titulo,
tb_anuncio.descricao,
tb_anuncio.id_categoria_imovel,
tb_anuncio.id_proposta_imovel,
tb_anuncio.id_estado,
tb_anuncio.id_cidade,
tb_anuncio.id_bairro,
tb_categoria_imovel.categoria,
tb_proposta_imovel.proposta,
tb_estado.uf,
tb_cidade.cidade,
tb_bairro.bairro,
tb_anuncio.numero_quartos,
tb_anuncio.numero_suites,
tb_anuncio.numero_banheiros,
tb_anuncio.numero_vagas,
tb_anuncio.metragem,
tb_anuncio.valor_imovel,
tb_anuncio.valor_condominio,
tb_anuncio.valor_iptu,
tb_anuncio.youtube,
tb_usuario.email_contato,
tb_usuario.telefone,
tb_usuario.exibir_telefone,
/*(SELECT imagem FROM tb_imagem WHERE id_anuncio = tb_anuncio.id_anuncio AND situacao = 1 ORDER BY id_imagem DESC LIMIT 1) AS 'imagem',*/
tb_usuario.logotipo,
tb_anuncio.situacao
FROM tb_anuncio
LEFT JOIN tb_usuario ON tb_usuario.id_usuario = tb_anuncio.id_usuario
LEFT JOIN tb_categoria_imovel ON tb_categoria_imovel.id_categoria_imovel = tb_anuncio.id_categoria_imovel
LEFT JOIN tb_proposta_imovel ON tb_proposta_imovel.id_proposta_imovel = tb_anuncio.id_proposta_imovel
LEFT JOIN tb_estado ON tb_estado.id_estado = tb_anuncio.id_estado
LEFT JOIN tb_cidade ON tb_cidade.id_cidade = tb_anuncio.id_cidade
LEFT JOIN tb_bairro ON tb_bairro.id_bairro = tb_anuncio.id_bairro