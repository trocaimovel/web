SELECT
tb_usuario.id_usuario,
tb_usuario.nome,
tb_usuario.sobrenome,
tb_usuario.nascimento AS 'nascimento_sql',
DATE_FORMAT(tb_usuario.nascimento, '%d/%m/%Y') AS 'nascimento',
tb_usuario.sexo,
tb_usuario.id_estado,
tb_estado.uf,
tb_usuario.id_cidade,
tb_cidade.cidade,
tb_usuario.cpf,
tb_usuario.rg,
tb_usuario.email_cadastro,
tb_usuario.email_contato,
tb_usuario.telefone,
tb_usuario.data_cadastro,
tb_usuario.data_cancelamento,
tb_usuario.exibir_email,
tb_usuario.exibir_telefone,
tb_usuario.logotipo,
tb_usuario.push,
tb_usuario.situacao
FROM tb_usuario
LEFT JOIN tb_estado ON tb_estado.id_estado = tb_usuario.id_estado
LEFT JOIN tb_cidade ON tb_cidade.id_cidade = tb_usuario.id_cidade