SELECT
tb_interesse_anuncio.id_interesse_anuncio,
tb_interesse_anuncio.id_anuncio,
tb_anuncio.id_usuario AS 'id_anunciante',
tb_interesse_anuncio.id_usuario,
tb_interesse_anuncio.`data`,
tb_interesse_anuncio.situacao
FROM tb_interesse_anuncio
LEFT JOIN tb_anuncio ON tb_anuncio.id_anuncio = tb_interesse_anuncio.id_anuncio