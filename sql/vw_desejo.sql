SELECT 
tb_desejo.id_desejo,
tb_desejo.id_usuario_interessado,
tb_usuario.nome AS 'nome_usuario_interessado',
tb_usuario.sobrenome AS 'sobrenome_usuario_interessado',
tb_desejo.id_anuncio_proprietario,
(SELECT imagem FROM tb_imagem WHERE numero = 1 AND id_anuncio = tb_desejo.id_anuncio_proprietario AND situacao = 1) AS 'imagem_anuncio_proprietario',
tb_desejo.id_anuncio_interessado,
tb_desejo.id_usuario_anunciante,
tb_desejo.`data`,
tb_desejo.situacao
FROM tb_desejo
LEFT JOIN tb_usuario ON tb_usuario.id_usuario = tb_desejo.id_usuario_interessado