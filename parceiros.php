<!DOCTYPE html>
<html lang="en">
<head>

<?php include("head.php"); ?>

<?php include("metatags.home.php") ?>

</head>
<body>
  
<?php include_once("analyticstracking.php") ?>

<?php include("navbar.php"); ?>

<div class="barra-titulo">
<div class="container">
<div class="row">
  <div class="col-md-12 text-left"><b>Anuncie hoje o seu imóvel no Troca Imóvel!</b></div>
</div>
</div>
</div>

<div class="pagina-interna">
<div class="container">
<div class="row">
<div class="col-md-3 hidden-xs">

<?php include("sidebar.pesquisa.php"); ?>

</div>
    
<div class="col-md-9">
<div class="row">
<div class="col-md-12"><b class="text-primary"><i class="fa fa-file-o fa-fw" aria-hidden="true"></i> Parceiros</b><hr class="hr-titulo"></div>
<div class="col-md-12 text-justify">

<div class="row">
  <!--<div class="col-md-4 col-sm-6 col-xs-12">
    <div class="panel panel-default">
      <div class="panel-heading">Fantástiko</div>
      <div class="panel-body">
        <a href="https://www.imobex.com.br" target="_blank"><img src="<?php print($murl); ?>/img/fantastiko.png" class="img-parceiro"></a>
      </div>
    </div>
  </div>-->
  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="panel panel-default">
      <div class="panel-heading">Imobex</div>
      <div class="panel-body">
        <a href="https://www.imobex.com.br" target="_blank"><img src="<?php print($murl); ?>/img/imobex.png" class="img-parceiro"></a>
      </div>
    </div>
  </div>
</div>

</div>
</div>
</div>
</div>
</div>

<?php include("footer.php"); ?>

<script src="<?php print($murl); ?>/js/bootstrap.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.money.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/script.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/pesquisa.min.js?<?php print($cache); ?>"></script>

</body>
</html>