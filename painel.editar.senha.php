<!DOCTYPE html>
<html lang="en">
<head>

<?php include("head.php"); ?>
<script>verificar_autenticacao(1);</script>

</head>
<body>

<?php include("navbar.php"); ?>

<?php include("conectado.php"); ?>

<div class="pagina-interna m-bottom-30">
<div class="container">
<div class="row">
<div class="col-md-3 hidden-sm hidden-xs">

<?php include("sidebar.usuario.php"); ?>

</div>

<div class="col-md-9">
<div class="row">
<div class="col-md-12"><b class="text-primary"><i class="fa fa-file-o fa-fw" aria-hidden="true"></i> Alterar Senha</b><hr class="hr-titulo"></div>

<div class="col-md-12 m-bottom-20">
<ul class="nav nav-tabs">
  <li><a href="<?php print($murl); ?>/painel.editar.dados"><span class="hidden-xs">Meus</span> Dados</a></li>
  <li><a href="<?php print($murl); ?>/painel.alterar.logo">Logotipo</a></li>
  <li class="active"><a href="<?php print($murl); ?>/painel.editar.senha"><span class="hidden-xs">Alterar</span> Senha</a></li>
  <li class="hidden-xs"><a href="<?php print($murl); ?>/painel.remover.conta">Remover Conta</a></li>
</ul>
</div>

<div class="col-md-3">
  <label>Senha atual:</label>
  <input type="password" class="form-control" id="senha-atual" name="senhaAtual" maxlength="20">
</div>
<div class="col-md-3">
  <p class="visible-xs  visible-sm"></p>
  <label>Nova senha:</label>
  <input type="password" class="form-control" id="nova-senha" name="novaSenha" maxlength="20">
</div>
<div class="col-md-3">
  <p class="visible-xs  visible-sm"></p>
  <label>Repetir nova senha:</label>
  <input type="password" class="form-control" id="repetir-senha" name="repetirSenha" maxlength="20">
</div>

<div class="col-md-12">
  <hr>
  <div class="alert alert-success display-none" id="alert-success"></div>
  <div class="alert alert-danger display-none" id="alert-danger"></div>
  <div class="alert alert-warning display-none" id="alert-warning"></div>
  <button class="btn btn-danger btn-lg" id="btn-submit" onclick="editar_senha_usuario();"><i class="fa fa-check fa-fw" aria-hidden="true"></i> Alterar Senha</button>
</div>

</div>
</div>
</div>
</div>
</div>

<script src="<?php print($murl); ?>/js/bootstrap.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/script.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/painel.min.js?<?php print($cache); ?>"></script>

</body>
</html>