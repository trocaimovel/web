<div class="modal fade" tabindex="-1" role="dialog" id="modal-chat">
<div class="modal-dialog">
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <b class="p-top-2 text-primary" id="chat-destinatario"></b>
    <input type="hidden" id="chat-id-destinatario">
  </div>
  <div class="modal-body modal-chat" id="chat-conversa"></div>
  <div class="modal-footer">
    <div class="input-group">
      <input type="text" class="form-control" id="chat-mensagem" name="chatMensagem" placeholder="escreva uma mensagem..." maxlength="150">
      <span class="input-group-btn"><button class="btn btn-primary" type="button" onclick="chat_enviar_mensagem($('#chat-id-destinatario').val());"><i class="fa fa-paper-plane-o fa-fw" aria-hidden="true"></i> Enviar</button></span>
    </div>
  </div>
</div>
</div>
</div>

<div class="display-none" id="template-chat">
<div class="chat-mensagem-{{classe}}"><b class="text-primary">{{remetente}}:</b><p class="pull-right text-muted">{{hora}}</p><br>{{mensagem}}</div>
</div>