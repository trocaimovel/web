<div class="sidebar-pesquisa hidden-xs hidden-sm">
  <div class="row">
  <form id="formulario-pesquisa">
    <div class="col-md-12">
      <label>Finalidade:</label>
      <select class="form-control" id="select-proposta" name="idProposta"></select>
    </div>
    <div class="col-md-12 m-top-10">
      <label>Imóvel:</label>
      <select class="form-control" id="select-categoria" name="idCategoria"></select>
    </div>
    <div class="col-md-6 m-top-10">
      <label>Valor. Mín:</label>
      <input type="text" class="form-control" id="valor-minimo" name="valorMinimo">
    </div>
    <div class="col-md-6 m-top-10">
      <label>Valor. Máx:</label>
      <input type="text" class="form-control" id="valor-maximo" name="valorMaximo">
    </div>
    <div class="col-md-6 m-top-10">
      <label>Quartos:</label>
      <select class="form-control" id="n-quartos" name="nQuartos">
        <option value="0"></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5+</option>
      </select>
    </div>
    <div class="col-md-6 m-top-10">
      <label>Suítes:</label>
      <select class="form-control" id="n-suites" name="nSuites">
        <option value="0"></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5+</option>  
      </select>
    </div>
    <div class="col-md-6 m-top-10" id="n-banheiros" name="nBanheiros">
      <label>Banheiros:</label>
      <select class="form-control">
        <option value="0"></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5+</option>
      </select>
    </div>
    <div class="col-md-6 m-top-10">
      <label>Vagas:</label>
      <select class="form-control" id="n-vagas" name="nVagas">
        <option value="0"></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5+</option>
      </select>
    </div>
    <div class="col-md-12 m-top-10">
      <label>Estado:</label>
      <select class="form-control" id="select-estado" name="idEstado" onchange="carregar_select_cidade('#formulario-pesquisa ', $(this).val());"></select>
    </div>
    <div class="col-md-12 m-top-10">
      <label>Cidade:</label>
      <select class="form-control" id="select-cidade" name="idCidade" onchange="carregar_select_bairro('#formulario-pesquisa ', $(this).val());"></select>
    </div>
    <div class="col-md-12 m-top-10">
      <label>Bairro:</label>
      <select class="form-control" id="select-bairro" name="idBairro"></select>
    </div>
    <div class="col-md-6 m-top-10">
      <label>Área. Mín:</label>
      <input type="number" class="form-control" id="metragem-minima" name="metragemMinima" placeholder="m²">
    </div>
    <div class="col-md-6 m-top-10">
      <label>Área. Máx:</label>
      <input type="number" class="form-control" id="metragem-maxima" name="metragemMaxima" placeholder="m²">
    </div>
    <div class="col-xs-12 m-top-15"><div class="btn btn-danger btn-lg btn-block" onclick="pesquisar();"><i class="fa fa-search fa-fw"></i> Pesquisar</div></div>
  </form>
  </div>
</div>