<!DOCTYPE html>
<html lang="en">
<head>

<?php include("head.php"); ?>

<?php include("metatags.home.php") ?>

</head>
<body class="body-bg">
  
<?php include_once("analyticstracking.php") ?>

<?php include("navbar.php"); ?>

<div class="banner">
<div class="container">
  <div class="row">
    <div class="col-md-8 hidden-sm hidden-xs"><img src="img/mobile.png" class="mobile"></div>
    <div class="col-md-4"><div class="pesquisa">
      <div class="row">
        <form id="formulario-pesquisa">
        <div class="col-xs-6">
          <select class="form-control m-bottom-20" id="select-proposta" name="idProposta" disabled="true">
            <option value="0">carregando...</option>
          </select>
        </div>
        <div class="col-xs-6">
          <select class="form-control m-bottom-20" id="select-categoria" name="idCategoria">
            <option value="0">carregando...</option>
          </select>
        </div>
        <div class="col-xs-12">
          <select class="form-control m-bottom-20" id="select-estado" name="idEstado" onchange="carregar_select_cidade('#formulario-pesquisa ', $(this).val());">
            <option value="0">carregando...</option>
          </select>
        </div>
        <div class="col-xs-12">
          <select class="form-control m-bottom-20" id="select-cidade" name="idCidade">
            <option value="0">carregando...</option>
          </select>
        </div>
        <div class="col-xs-6">
          <select class="form-control m-bottom-20" id="select-quartos" name="nQuartos">
            <option value="0">nº de quartos</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5+</option>
          </select>
        </div>
        <div class="col-xs-6">
          <select class="form-control m-bottom-20" id="select-suites" name="nSuites">
            <option value="0">nº de suítes</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5+</option>            
          </select>
        </div>
        <div class="col-xs-12"><div class="btn btn-danger btn-lg btn-block" onclick="pesquisar();"><i class="fa fa-search fa-fw"></i> Pesquisar</div></div>
      </form>
      </div>
    </div></div>
  </div>
</div>
</div>

<div class="barra-destaque">
<div class="container">
  <div class="row">
    <div class="col-md-3 item-destaque">
      <h2><i class="fa fa-search fa-fw text-danger"></i> Pesquise Imóveis</h2>
      Pesquise por imóveis para trocar, vender ou alugar em todo o Brasil. Entre em contato direto com o anunciante através de e-mail, telefone ou chat.
    </div>
    <div class="col-md-3 item-destaque">
      <h2><i class="fa fa-home fa-fw text-danger"></i> Negocie Imóveis</h2>
      Marque imóveis que você tem interesse em negociar, caso o anunciante também se interesse em um dos seus, você será notificado. 
    </div>
    <div class="col-md-3 item-destaque">
      <h2><i class="fa fa-asterisk fa-fw text-danger"></i> Cadastre Interesses</h2>
      Cadastre seus interesses, assim podemos exibir anúncios relevantes para você, com a porcentagem de compatibilidade com o que você procura.
    </div>
    <div class="col-md-3 item-destaque">
      <h2><i class="fa fa-mobile fa-fw text-danger"></i> Troca Imóvel Mobile</h2>
      O Troca Imóvel também está disponível para dispositivos móveis, com versão para iOS ou Android. Instale agora no seu celular ou tablet.
    </div>
  </div>
</div>
</div>

<div class="container">
<div class="row destaque">
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="borda-cinza m-bottom-30">
      <a href="<?php print($murl); ?>/resultado.pesquisa?idCidade=9668"><div class="destaque-quadro">
        <div class="destaque-imovel-localizacao">Imóveis em São Paulo</div>
        <div class="destaque-imovel" style="background-image: url(img/cidade-saopaulo.jpg);"></div>
      </div></a>
    </div>
  </div>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="borda-cinza m-bottom-30">
      <a href="<?php print($murl); ?>/resultado.pesquisa?idCidade=7043"><div class="destaque-quadro">
        <div class="destaque-imovel-localizacao">Imóveis no Rio de Janeiro</div>
        <div class="destaque-imovel" style="background-image: url(img/cidade-riodejaneiro.jpg);"></div>
      </div></a>
    </div>
  </div>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="borda-cinza m-bottom-30">
      <a href="<?php print($murl); ?>/resultado.pesquisa?idCidade=6015"><div class="destaque-quadro">
        <div class="destaque-imovel-localizacao">Imóveis em Curitiba</div>
        <div class="destaque-imovel" style="background-image: url(img/cidade-curitiba.jpg);"></div>
      </div></a>
    </div>
  </div>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="borda-cinza m-bottom-30">
      <a href="<?php print($murl); ?>/resultado.pesquisa?idCidade=2754"><div class="destaque-quadro">
        <div class="destaque-imovel-localizacao">Imóveis em Belo Horizonte</div>
        <div class="destaque-imovel" style="background-image: url(img/cidade-belohorizonte.jpg);"></div>
      </div></a>
    </div>
  </div>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="borda-cinza m-bottom-30">
      <a href="<?php print($murl); ?>/resultado.pesquisa?idCidade=2048"><div class="destaque-quadro">
        <div class="destaque-imovel-localizacao">Imóveis em Vitória</div>
        <div class="destaque-imovel" style="background-image: url(img/cidade-vitoria.jpg);"></div>
      </div></a>
    </div>
  </div>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="borda-cinza m-bottom-30">
      <a href="<?php print($murl); ?>/resultado.pesquisa?idCidade=8452"><div class="destaque-quadro">
        <div class="destaque-imovel-localizacao">Imóveis em Florianopolis</div>
        <div class="destaque-imovel" style="background-image: url(img/cidade-florianopolis.jpg);"></div>
      </div></a>
    </div>
  </div>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="borda-cinza m-bottom-30">
      <a href="<?php print($murl); ?>/resultado.pesquisa?idCidade=988"><div class="destaque-quadro">
        <div class="destaque-imovel-localizacao">Imóveis em Salvador</div>
        <div class="destaque-imovel" style="background-image: url(img/cidade-salvador.jpg);"></div>
      </div></a>
    </div>
  </div>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="borda-cinza m-bottom-30">
      <a href="<?php print($murl); ?>/resultado.pesquisa?idCidade=5406"><div class="destaque-quadro">
        <div class="destaque-imovel-localizacao">Imóveis em Recife</div>
        <div class="destaque-imovel" style="background-image: url(img/cidade-recife.jpg);"></div>
      </div></a>
    </div>
  </div>
</div>
</div>

<div class="contador">
<div class="container">
<!--  <div class="row">
    <div class="col-xs-12 text-center contador-total">São mais de <b class="total-imoveis">0</b> para trocar, vender, ou alugar!</div>
  </div>-->
<div class="row">
  <div class="col-md-2 col-sm-3 col-xs-6 text-center">
    <div class="contador-valor total-apartamentos">0</div>
    <div class="contador-rotulo">Apartamentos</div>
  </div>
  <div class="col-md-2 col-sm-3 col-xs-6 text-center">
    <div class="contador-valor total-casas">0</div>
    <div class="contador-rotulo">Casas</div>
  </div>
  <div class="m-top-10 visible-xs"></div>
  <div class="col-md-2 col-xs-6 hidden-sm text-center">
    <div class="contador-valor total-flats">0</div>
    <div class="contador-rotulo">Flats</div>
  </div>
  <div class="m-top-10 visible-xs"></div>
  <div class="col-md-2 col-xs-6 hidden-sm text-center">
    <div class="contador-valor total-loteamentos">0</div>
    <div class="contador-rotulo">Loteamentos</div>
  </div>
  <div class="m-top-10 visible-xs"></div>
  <div class="col-md-2 col-sm-3 col-xs-6 text-center">
    <div class="contador-valor total-lojas">0</div>
    <div class="contador-rotulo">Lojas</div>
  </div>
  <div class="m-top-10 visible-xs"></div>
  <div class="col-md-2 col-sm-3 col-xs-6 text-center">
    <div class="contador-valor" id="total-salas">0</div>
    <div class="contador-rotulo">Salas<span class="hidden-sm hidden-xs"> Comerciais</span></div>
  </div>
</div>
</div>
</div>

<!--<div class="publicidade-home" id="adblock">
<div class="container">
<div class="row">
  <div class="col-md-3 col-sm-3">
    <img src="http://placehold.it/250x250" class="ad-250">
  </div>
  <div class="m-top-30 visible-xs"></div>
  <div class="col-md-3 col-sm-3">
    <img src="http://placehold.it/250x250" class="ad-250">
  </div>
  <div class="m-top-30 visible-xs"></div>
  <div class="col-md-3 col-sm-3">
    <img src="http://placehold.it/250x250" class="ad-250">
  </div>
  <div class="m-top-30 visible-xs"></div>
  <div class="col-md-3 col-sm-3">
    <img src="http://placehold.it/250x250" class="ad-250">
  </div>
</div>
</div>
</div>
-->

<?php include("footer.php"); ?>

<script src="<?php print($murl); ?>/js/bootstrap.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.money.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/script.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/pesquisa.min.js?<?php print($cache); ?>"></script>
<script>
carregar_contador_home();
</script>

</body>
</html>