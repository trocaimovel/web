<!DOCTYPE html>
<html lang="en">
<head>

<?php include("head.php"); ?>
<script>verificar_autenticacao(1);</script>

</head>
<body>

<?php include("navbar.php"); ?>

<?php include("conectado.php"); ?>

<div class="pagina-interna">
<div class="container">
  
<div class="row">
<div class="col-md-3 hidden-sm hidden-xs">

<?php include("sidebar.usuario.php"); ?>

</div>
    
<div class="col-md-9">
<div class="row"><div class="col-md-12"><b class="text-primary"><i class="fa fa-file-o fa-fw" aria-hidden="true"></i> Recomendados</b><hr class="hr-titulo"></div></div>
<div class="row">
<div class="col-md-12">
  <div class="alert alert-danger display-none" id="alert-danger">Nenhum imóvel que possa te interessar foi encontrado.</div>
  <div class="alert alert-info" id="alert-info"><i class="fa fa-refresh fa-spin fa-fw"></i> Aguarde, carregando...</div>
</div>
</div>
<div class="display-none" id="grid-resultado"></div>
</div>
    
</div>
</div>
</div>

<div class="display-none" id="template">
<div class="item-resultado hidden-sm hidden-xs">
<div class="row">
  <div class="col-md-4">
    <div class="borda-cinza">
    <a href="<?php print($murl); ?>/anuncio/{{id}}"><div class="destaque-quadro">
      <div class="destaque-imovel-valor">R$ {{valor}}</div>
      <div class="destaque-imovel" style="background-image: url({{imagem}});"></div>
      <div class="item-resultado-porcentagem">{{pontuacao}}%</div>
    </div></a>
  </div></div>
  <div class="col-md-5">
    <a href="<?php print($murl); ?>/anuncio/{{id}}">
      <div class="item-resultado-endereco text-primary">{{titulo}}</div>
      <div class="item-resultado-localizacao text-default">{{bairro}}, {{cidade}} - {{uf}}</div>
      <div class="item-resultado-descricao text-default" id="descricao">{{descricao}}</div>
    </a>
  </div>
  <div class="col-md-3">
    <ul class="list-group">
      <li class="list-group-item"><span class="badge">{{nQuartos}}</span><i class="fa fa-bed fa-fw text-primary" aria-hidden="true"></i> Quartos</li>
      <li class="list-group-item"><span class="badge">{{nSuites}}</span><i class="fa fa-bed fa-fw text-danger" aria-hidden="true"></i> Suítes</li>
      <li class="list-group-item"><span class="badge">{{nBanheiros}}</span><i class="fa fa-shower fa-fw text-danger" aria-hidden="true"></i> Banheiros</li>
      <li class="list-group-item"><span class="badge">{{nVagas}}</span><i class="fa fa-car fa-fw text-danger" aria-hidden="true"></i> Vagas</li>
    </ul>
  </div>
</div>
</div>

<div class="item-resultado visible-sm visible-xs">
<div class="row">
  <div class="col-sm-6 col-xs-12">
    <div class="borda-cinza">
      <a href="<?php print($murl); ?>/anuncio/{{id}}"><div class="destaque-quadro">
        <div class="destaque-imovel-valor">R$ {{valor}}</div>
        <div class="destaque-imovel-dados">
          <div class="destaque-imovel-icone"><i class="fa fa-bed fa-fw" aria-hidden="true"></i> {{nQuartos}}</div>
          <div class="destaque-imovel-icone"><i class="fa fa-shower" aria-hidden="true"></i> {{nBanheiros}}</div>
          <div class="destaque-imovel-icone"><i class="fa fa-car fa-fw" aria-hidden="true"></i> {{nVagas}}</div>
        </div>
        <div class="destaque-imovel" style="background-image: url({{imagem}});"></div>
      </div></a>
    </div>
  </div>
  <div class="col-sm-6 col-xs-12">
    <a href="<?php print($murl); ?>/anuncio/{{id}}">
      <div class="visible-xs m-top-10"></div>
      <div class="item-resultado-endereco text-primary">{{titulo}}</div>
      <div class="item-resultado-localizacao text-default">{{bairro}}, {{cidade}} - {{uf}}</div>
      <div class="item-resultado-descricao text-default" id="descricao">{{descricao}}</div>
    </a>
  </div>
</div>
</div>

<hr>
</div>

<script src="<?php print($murl); ?>/js/bootstrap.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/script.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/painel.min.js?<?php print($cache); ?>"></script>
<script>
carregar_anuncios_recomendados();
</script>

</body>
</html>