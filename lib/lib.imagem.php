<?php
//require("wideimage/WideImage.php");

function imagem_redimensionar($diretorio, $arquivo, $extencao, $x, $y){
  $extencao = str_replace(".", "", $extencao);
	$arquivo = $diretorio."/".$arquivo.".".$extencao;
	$img = WideImage::load($arquivo);
	$img = $img->resize($x, $y, "outside");
	$img = $img->crop(0, 0, $x, $y);
	$img->saveToFile($arquivo);
	$img->destroy();
  if(file_exists($arquivo)) return true;
  else return false;
}

function imagem_gerar_thumb($diretorio, $arquivo, $extencao, $x, $y){
	$extencao = str_replace(".", "", $extencao);
	$img = WideImage::load($diretorio."/".$arquivo.".".$extencao);
	$img = $img->resize($x, $y, "outside");
	$img = $img->crop(0, 0, $x, $y);
	$img->saveToFile($diretorio."/".$arquivo."-thumb.".$extencao);
	$img->destroy();
  if(file_exists($diretorio."/".$arquivo."-thumb.".$extencao)) return true;
  else return false;
}

?>