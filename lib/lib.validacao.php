<?php

function valida_nome($nome){
  if(strlen($nome) < 2) return false;
  else return true;
}

function valida_url($url){
  if(!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $url)) return false;
  else return true;
}

function valida_url_youtube($url){
  if(strlen($url) != 0){
    if((substr_count($url, "youtube.com") != 1) && (substr_count($url, "youtu.be") != 1)) return false;
    else return true;
  }
  else return true;
}

function valida_data($data){
  if(substr_count($data, "/") == 2){
    $data = explode("/", $data);
    if(checkdate($data[1], $data[0], $data[2])) return true;
    else return false;
  }
  else return false;
}

function valida_data_nascimento($data){
  if(substr_count($data, "/") == 2){
    $data = explode("/", $data);
    if(checkdate($data[1], $data[0], $data[2])){
      if(mktime(0, 0, 0, $data[1], $data[0], $data[2]) < time()) return true;
      else return false;
    }
    else return false;
  }
  else return false;
}

function valida_sexo($sexo){
  $sexo = strtolower($sexo);
  if(($sexo != "m") && ($sexo != "f") && ($sexo != "masculino") && ($sexo != "feminino")) return false;
  else return true;
}

// Função para verificar se o CPF informado pertence ao usuário cadastrado com um determinado e-mail.
function valida_cpf_usuario($cpf, $emailCadastro){
  include("../database.php");
  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  $query = mysqli_query($link, "SELECT COUNT(*) AS 'total' FROM tb_usuario WHERE email_cadastro = '{$emailCadastro}' AND cpf = '{$cpf}' AND situacao = 1");
  $col = mysqli_fetch_assoc($query);
  mysqli_close($link);
  $total = $col["total"];
  
  if($total == 1) return true;
  else return false;
}

function valida_senha_usuario($senha, $idUsuario){
  include("../database.php");
  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  $query = mysqli_query($link, "SELECT COUNT(*) AS 'total' FROM tb_usuario WHERE senha = '{$senha}' AND id_usuario = $idUsuario AND situacao = 1");
  $col = mysqli_fetch_assoc($query);
  mysqli_close($link);
  $total = $col["total"];
  
  if($total == 1) return true;
  else return false;
}

function valida_cpf($cpf, $idUsuario, $acao){
  include("../database.php");
  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  
  if($acao == 1) $query = mysqli_query($link, "SELECT COUNT(*) AS 'total' FROM tb_usuario WHERE cpf = '{$cpf}' AND id_usuario != $idUsuario AND situacao = 1");
  else $query = mysqli_query($link, "SELECT COUNT(*) AS 'total' FROM tb_usuario WHERE cpf = '{$cpf}' AND situacao = 1");
  
  $col = mysqli_fetch_assoc($query);
  mysqli_close($link);
  $total = $col["total"];
  
  $validacao = true;
  if((empty($cpf)) || (strlen($cpf) != 14)) $validacao = false;
  $cpf = str_replace(".", "", $cpf);
  $cpf = str_replace("-", "", $cpf);
  $cpf = str_pad($cpf, 11, "0", STR_PAD_LEFT);
  
  if(
    $cpf == "00000000000" || $cpf == "11111111111" || $cpf == "22222222222" || 
    $cpf == "33333333333" || $cpf == "44444444444" || $cpf == "55555555555" || 
    $cpf == "66666666666" || $cpf == "77777777777" || $cpf == "88888888888" || 
    $cpf == "99999999999") $validacao = false;
  else{    
    for($t = 9; $t < 11; $t++){
      for($d = 0, $c = 0; $c < $t; $c++) $d += $cpf{$c} * (($t + 1) - $c);
      $d = ((10 * $d) % 11) % 10;
      if ($cpf{$c} != $d) $validacao = false;
    }
  }

  if($total == 0 && $validacao == true) return true;
  else return false;
}

function valida_rg($rg, $idUsuario, $acao){
  include("../database.php");
  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  
  if($acao == 1) $query = mysqli_query($link, "SELECT COUNT(*) AS 'total' FROM tb_usuario WHERE rg = '{$rg}' AND id_usuario != $idUsuario AND situacao = 1");
  else $query = mysqli_query($link, "SELECT COUNT(*) AS 'total' FROM tb_usuario WHERE rg = '{$rg}' AND situacao = 1");
  
  $col = mysqli_fetch_assoc($query);
  mysqli_close($link);
  $total = $col["total"];

  $rg = str_replace(".", "", $rg);
  $rg = str_replace("-", "", $rg);
  if((strlen($rg) < 5) && (!is_int($rg))) $validacao = false;
  else $validacao = true;
  
  if($total == 0 && $validacao == true) return true;
  else return false;
}

function valida_email($email){
  if(filter_var($email, FILTER_VALIDATE_EMAIL)) return true;
  else return false;
}

function valida_email_cadastro($emailCadastro, $idUsuario, $acao){
  include("../database.php");
  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);

  if($acao == 1) $query = mysqli_query($link, "SELECT COUNT(*) AS 'total' FROM tb_usuario WHERE email_cadastro = '{$emailCadastro}' AND id_usuario != $idUsuario AND situacao = 1");
  else $query = mysqli_query($link, "SELECT COUNT(*) AS 'total' FROM tb_usuario WHERE email_cadastro = '{$emailCadastro}' AND situacao = 1");
  
  $col = mysqli_fetch_assoc($query);
  mysqli_close($link);
  $total = $col["total"];
    
  if(!filter_var($emailCadastro, FILTER_VALIDATE_EMAIL)) $validacao = false;
  else $validacao = true;
  
  if($total == 0 && $validacao == true) return true;
  else return false;
}

function valida_email_contato($emailContato, $idUsuario, $acao){
  include("../database.php");
  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  
  if($acao == 1) $query = mysqli_query($link, "SELECT COUNT(*) AS 'total' FROM tb_usuario WHERE email_contato = '{$emailContato}' AND id_usuario != $idUsuario AND situacao = 1");
  else $query = mysqli_query($link, "SELECT COUNT(*) AS 'total' FROM tb_usuario WHERE email_contato = '{$emailContato}' AND situacao = 1");
  
  $col = mysqli_fetch_assoc($query);
  mysqli_close($link);
  $total = $col["total"];
  
  if(!filter_var($emailContato, FILTER_VALIDATE_EMAIL)) $validacao = false;
  else $validacao = true;
  
  if($total == 0 && $validacao == true) return true;
  else return false;
}

// Funções específicas do Troca Imóvel

function valida_desejo_anuncio($idUsuarioInteressado, $idAnuncioInteressado, $idAnuncioProprietario){
  include("../database.php");
  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  $query = mysqli_query($link, "SELECT COUNT(*) AS 'total' FROM tb_desejo WHERE id_usuario_interessado = $idUsuarioInteressado AND id_anuncio_proprietario = $idAnuncioProprietario AND id_anuncio_interessado = $idAnuncioInteressado AND situacao = 1");
  $col = mysqli_fetch_assoc($query);
  mysqli_close($link);
  
  if($col["total"] == 0) return true;
  else return false;
}

function valida_usuario_anunciante($idAnuncioInteressado){
  include("../database.php");
  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  $query = mysqli_query($link, "SELECT id_usuario FROM tb_anuncio WHERE id_anuncio = $idAnuncioInteressado AND situacao = 1");
  $col = mysqli_fetch_assoc($query);
  $idUsuarioAnunciante = (int)$col["id_usuario"];
  mysqli_close($link);
  
  if($idUsuarioAnunciante == 0) return false;
  else return $idUsuarioAnunciante;
}

function valida_combinacao($idUsuarioInteressado, $idAnuncioInteressado, $idAnuncioProprietario, $idUsuarioAnunciante){
  include("../database.php");
  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  $query = mysqli_query($link, "SELECT COUNT(*) AS 'total' FROM vw_desejo WHERE id_usuario_interessado = $idUsuarioAnunciante AND id_anuncio_proprietario = $idAnuncioInteressado AND id_anuncio_interessado = $idAnuncioProprietario AND id_usuario_anunciante = $idUsuarioInteressado AND situacao = 1");
  $col = mysqli_fetch_assoc($query);
  $total = (int)$col["total"];
  mysqli_close($link);
  
  if($total == 0) return false;
  else return true;
}


function valida_facebook($email, $idFacebok){
  include("../database.php");
  $link = mysqli_connect($dbServidor, $dbUsuario, $dbSenha, $dbBanco);
  $query = mysqli_query($link, "SELECT id_usuario, id_facebook FROM tb_usuario WHERE email_cadastro = '{$email}' AND situacao = 1");
  $col = mysqli_fetch_assoc($query);
  $idUsuario = (int)$col["id_usuario"];
  $retornoIdFacebook = $col["id_facebook"];
  
  if(($idUsuario != 0) && ($idFacebok == $retornoIdFacebook)) return 1;
  elseif(($idUsuario != 0) && ($idFacebok != $retornoIdFacebook)) return 2;
  else return 3;
  mysqli_close($link);
}
?>