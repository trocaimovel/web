<!DOCTYPE html>
<html lang="en">
<head>

<?php include("head.php"); ?>
<script>verificar_autenticacao(1);</script>
<link href="<?php print($murl); ?>/css/themes/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet">
<link href="<?php print($murl); ?>/css/jquery.filer.css" type="text/css" rel="stylesheet">
<script src="<?php print($murl); ?>/js/jquery.filer.min.js"></script>

</head>
<body>

<?php include("navbar.php"); ?>

<?php include("conectado.php"); ?>

<div class="pagina-interna m-bottom-30">
<div class="container">
  <div class="row">
    <div class="col-md-3 hidden-sm hidden-xs">
      <?php include("sidebar.usuario.php"); ?>
    </div>
    <div class="col-md-9">
      <div class="row">
        <div class="col-md-12"><b class="text-primary"><i class="fa fa-file-o fa-fw" aria-hidden="true"></i> Publicar Anúncio</b><hr class="hr-titulo"></div>
        <form id="formulario-anunciar-imovel" method="post" enctype="multipart/formdata">
        <input type="hidden" id="token" name="token">
        <div class="col-md-12">
          <label>Título do anúncio:</label>
          <input type="text" class="form-control" id="titulo" name="titulo" maxlength="150">
        </div>
        <div class="col-md-12 m-top-10">
          <label>Descrição do imóvel:</label>
          <textarea class="form-control" rows="3" id="descricao" name="descricao" maxlength="500"></textarea>
        </div>
        <div class="col-md-6 m-top-10">
          <label>Categoria:</label>
          <select class="form-control" id="select-categoria" name="idCategoria"></select>
        </div>
        <div class="col-md-6 m-top-10">
          <label>Proposta:</label>
          <select class="form-control" id="select-proposta" name="idProposta"></select>
        </div>
        <div class="col-md-4 m-top-10">
          <label>Estado:</label>
          <select class="form-control" id="select-estado" name="idEstado" onchange="carregar_select_cidade(null, $(this).val());"></select>
        </div>
        <div class="col-md-4 m-top-10">
          <label>Cidade:</label>
          <select class="form-control" id="select-cidade" name="idCidade" onchange="carregar_select_bairro(null, $(this).val());"></select>
        </div>
        <div class="col-md-4 m-top-10">
          <label>Bairro:</label>
          <select class="form-control" id="select-bairro" name="idBairro"></select>
        </div>
        <div class="col-md-3 m-top-10">
          <label>Nº Quartos:</label>
          <input type="number" class="form-control" id="n-quartos" name="nQuartos" min="0" max="99">
        </div>
        <div class="col-md-3 m-top-10">
          <label>Nº Suítes:</label>
          <input type="number" class="form-control" id="n-suites" name="nSuites" min="0" max="99">
        </div>
        <div class="col-md-3 m-top-10">
          <label>Nº Banheiros:</label>
          <input type="number" class="form-control" id="n-banheiros" name="nBanheiros" min="0" max="99">
        </div>
        <div class="col-md-3 m-top-10">
          <label>Nº Vagas:</label>
          <input type="number" class="form-control" id="n-vagas" name="nVagas" min="0" max="99">
        </div>
        <div class="col-md-3 m-top-10">
          <label>Área:</label>
          <input type="number" class="form-control" id="metragem" name="metragem" min="0" max="99999" placeholder="em metros quadrados">
        </div>
        <div class="col-md-3 m-top-10">
          <label>Valor do imóvel:</label>
          <input type="text" class="form-control" id="valor-imovel" name="valorImovel" maxlength="15">
        </div>
        <div class="col-md-3 m-top-10">
          <label>Valor do condomínio:</label>
          <input type="text" class="form-control" id="valor-condominio" name="valorCondominio" maxlength="10" placeholder="mensalidade">
        </div>
        <div class="col-md-3 m-top-10">
          <label>Valor do IPTU:</label>
          <input type="text" class="form-control" id="valor-iptu" name="valorIPTU" maxlength="10">
        </div>
        <div class="col-md-12 m-top-10">
          <label>Selecione uma ou mais fotografias do imóvel:</label>
          <input type="file" name="upload[]" id="filer-input" multiple="multiple">
        </div>
        <div class="col-md-12">
          <label>URL do vídeo no Youtube:</label>
          <input type="text" class="form-control" id="video-youtube" name="videoYoutube">
        </div>
        <div class="col-md-12">
          <hr>
          <div class="alert alert-success display-none" id="alert-success"></div>
          <div class="alert alert-danger display-none" id="alert-danger"></div>
          <div class="alert alert-warning display-none" id="alert-warning"></div>
          <div class="alert alert-info display-none" id="alert-loading"><i class="fa fa-refresh fa-spin fa-fw"></i> Aguarde, carregando...</div>
          <button class="btn btn-danger btn-lg"><i class="fa fa-bullhorn fa-fw" aria-hidden="true"></i> Publicar Anúncio</button>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>

<script src="<?php print($murl); ?>/js/bootstrap.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/bootstrap.filestyle.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/jquery.mask.money.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/script.min.js?<?php print($cache); ?>"></script>
<script src="<?php print($murl); ?>/js/painel.min.js?<?php print($cache); ?>"></script>
<script>
$(document).ready(function() {
carregar_select_estado();
carregar_select_cidade();
carregar_select_bairro();
carregar_select_categoria();
carregar_select_proposta();

$('#n-quartos').keypress(verificar_digito);
$('#n-suites').keypress(verificar_digito);
$('#n-banheiros').keypress(verificar_digito);
$('#n-vagas').keypress(verificar_digito);
$('#metragem').keypress(verificar_digito);

$('#valor-imovel').maskMoney({showSymbol:true, symbol:"R$", decimal:",", thousands:"."});
$('#valor-condominio').maskMoney({showSymbol:true, symbol:"R$", decimal:",", thousands:"."});
$('#valor-iptu').maskMoney({showSymbol:true, symbol:"R$", decimal:",", thousands:"."});

$('#filer-input').filer({
  addMore: true,
  limit: 50,
  maxSize: 2,
  extensions: ['jpg', 'jpeg', 'png', 'gif'],
  changeInput: true,
  showThumbs: true
});       
});
</script>

</body>
</html>