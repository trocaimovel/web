<?php
header("Access-Control-Allow-Origin: *");
include("database.php");
include("lib/lib.token.php");

if(isset($_POST["token"])){
  $token = strip_tags($_POST["token"]);
  $idUsuario = (int)retorna_id_usuario($token);
  if($idUsuario > 0) echo('[{"codigo":"100", "alerta":"Usuário autenticado com sucesso.", "token":"'.$token.'"}]');
  else echo('[{"codigo":"300", "alerta":"A autenticação do usuário falhou."}]'); 
}
else echo('[{"codigo":"300", "alerta":"A autenticação do usuário falhou."}]');
?>