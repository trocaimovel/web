<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Troca Imóvel</title>
<link href="<?php print($murl); ?>/css/bootstrap.css" rel="stylesheet">
<link href="<?php print($murl); ?>/css/style.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php print($murl); ?>/css/custom-font-icons.min.css" rel="stylesheet">
<link rel="icon" type="image/x-icon" href="<?php print($murl); ?>/favicon.ico" />
<script src="<?php print($murl); ?>/js/jquery-2.2.4.min.js"></script>
<script src="<?php print($murl); ?>/js/seguranca.min.js"></script>
