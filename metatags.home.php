<meta name="keywords" content="imóveis, casas, apartamentos, lojas, alugar, comprar, vender, trocar"/>
<meta name="description" content="Todas as formas de negociar o seu imóvel, em um só aplicativo.">
<meta name="subject" content="Todas as formas de negociar o seu imóvel, em um só aplicativo.">
<meta name="copyright" content="Troca Imóvel">
<meta name="language" content="pt-br">
<meta name="robots" content="index,follow">
<meta name="author" content="Troca Imóvel, contato@trocaimovel.com.br">
<meta name="reply-to" content="contato@trocaimovel.com.br">
<meta name="url" content="http://trocaimovel.com.br">
<meta name="identifier-URL" content="http://trocaimovel.com">
<meta http-equiv="Expires" content="0">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">

<meta name="og:title" content="Troca Imóvel"/>
<meta name="og:url" content="http://trocaimovel.com.br"/>
<meta name="og:image" content="http://trocaimovel.com.br/img/og.png"/>
<meta name="og:site_name" content="Troca Imóvel"/>
<meta name="og:description" content="Todas as formas de negociar o seu imóvel, em um só aplicativo."/>